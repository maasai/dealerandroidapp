package com.robisignals.dealer.test;

import android.test.suitebuilder.TestSuiteBuilder;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * TODO: Add a class header comment!
 */
public class FullTestSuit extends TestSuite {

    public static Test suit(){
        return new TestSuiteBuilder(FullTestSuit.class)
                .includeAllPackagesUnderHere().build();
    }

    public FullTestSuit() {
        super();
    }
}
