package com.robisignals.dealer.test;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.robisignals.dealer.provider.DealerContract;
import com.robisignals.dealer.provider.DealerDbHelper;

/**
 * TODO: Add a class header comment!
 */
public class TestDb extends AndroidTestCase {

    public static final String LOG_TAG = TestDb.class.getSimpleName();

    public void testCreateDb() throws  Throwable {
        //mContext.deleteDatabase(DealerDbHelper.DATABASE_NAME);

        SQLiteDatabase db  = new DealerDbHelper(this.mContext).getWritableDatabase();

        assertEquals(true, db.isOpen());
        db.close();

    }


    /*public void testInsertReadDb(){

        //test data
        String testColorId = "37393352-ca05-4823-b9c0-98bfc889f37d";
        String testColorName = "Blue";

        String testCreatedAt = "2015-04-06 06:29:54.000000";

        DealerDbHelper dbHelper  = new DealerDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DealerContract.ColorsEntry.COLOR_ID, testColorId);
        values.put(DealerContract.ColorsEntry.COLOR_NAME, testColorName);
        values.put(DealerContract.ColorsEntry.CREATED_AT, testCreatedAt);


       Uri affectedUri = mContext.getContentResolver().insert(DealerContract.ColorsEntry.CONTENT_URI, values);
        long coloRowId;

        //coloRowId = db.insert(DealerDbHelper.Tables.COLORS, null, values);

        //verify we got color row id back after insert
        //assertTrue(coloRowId != -1);
       //Log.d(LOG_TAG, "New row id " + coloRowId );

       Log.d(LOG_TAG, "Affected uri is  " + affectedUri );


        //columns needed

        String[] columns  = {
                DealerContract.ColorsEntry._ID,
                DealerContract.ColorsEntry.COLOR_ID,
                DealerContract.ColorsEntry.COLOR_NAME,
                DealerContract.ColorsEntry.CREATED_AT
        };

        //Get some data from db

     *//*   Cursor cursor = db.query(
                DealerDbHelper.Tables.COLORS,
                columns,
                null,//where columns
                null,//where values
                null,//group by columns
                null,//filter by row groups
                null//sort order
        );*//*

        Cursor cursor = mContext.getContentResolver().query(DealerContract.ColorsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (cursor.moveToFirst()){
            int colorIdIndex = cursor.getColumnIndex(DealerContract.ColorsEntry.COLOR_ID);
            String colorID = cursor.getString(colorIdIndex);

            int colorNameIndex = cursor.getColumnIndex(DealerContract.ColorsEntry.COLOR_NAME);
            String colorName = cursor.getString(colorNameIndex);

            int colorCreatedAtIndex = cursor.getColumnIndex(DealerContract.ColorsEntry.CREATED_AT);
            String colorCreatedAt = cursor.getString(colorCreatedAtIndex);

            Log.d(LOG_TAG, "The inserted color name is...: " + colorName );

            assertEquals(testColorId, colorID);
            assertEquals(testColorName, colorName);
            assertEquals(testCreatedAt, colorCreatedAt);


        }else
        {
            fail("No values returned");
        }


    }*/

   public void testReadDb(){
       Cursor cursor = mContext.getContentResolver().query(DealerContract.ListingsEntry.CONTENT_URI,
               null,
               null,
               null,
               null);

       Log.d(LOG_TAG, "The cursor has:...: columns are::" + cursor.getColumnCount() + "\n" + DatabaseUtils.dumpCursorToString(cursor) );

       for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {

//           int colorIdIndex = cursor.getColumnIndex(DealerContract.ColorsEntry.COLOR_ID);
//           String colorID = cursor.getString(colorIdIndex);

          /* int listingIdIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.LISTING_ID);
           String listingId = cursor.getString(listingIdIndex);

           int colorIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.COLOR_ID);
           String colorId = cursor.getString(colorIndex);
*/
          /* int priceIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.PRICE);
           String price = cursor.getString(priceIndex);

           int makeIdIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.MAKE_ID);
           String makeId = cursor.getString(makeIdIndex);

           int makeNameIndex = cursor.getColumnIndex("make_name");
           String makeName = cursor.getString(makeNameIndex);


//           int colorCreatedAtIndex = cursor.getColumnIndex(DealerContract.ColorsEntry.CREATED_AT);
//           String colorCreatedAt = cursor.getString(colorCreatedAtIndex);

      *//*     Log.d(LOG_TAG, "Listing Id is...: " + listingId );
           Log.d(LOG_TAG, "Color Id is...: " + colorId );*//*
           Log.d(LOG_TAG, "Price is...: " + price );
           Log.d(LOG_TAG, "Make id is...: " + makeId );
           Log.d(LOG_TAG, "Make name is...: " + makeName );*/





       }/*else
       {
           fail("No values returned");
       }*/

    }


}
