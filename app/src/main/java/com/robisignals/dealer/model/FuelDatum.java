package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class FuelDatum {
    @SerializedName("fuel_id")
    @Expose
    private String fuelId;
    @SerializedName("fuel_name")
    @Expose
    private String fuelName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The fuelId
     */
    public String getFuelId() {
        return fuelId;
    }

    /**
     *
     * @param fuelId
     * The fuel_id
     */
    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    /**
     *
     * @return
     * The fuelName
     */
    public String getFuelName() {
        return fuelName;
    }

    /**
     *
     * @param fuelName
     * The fuel_name
     */
    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
