package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class VehicleImageDatum {

    @SerializedName("vehicle_image_id")
    @Expose
    private String vehicleImageId;
    @SerializedName("filename")
    @Expose
    private String filename;
    @SerializedName("listing_id")
    @Expose
    private String listingId;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    public String getVehicleImageId() {
        return vehicleImageId;
    }

    public void setVehicleImageId(String vehicleImageId) {
        this.vehicleImageId = vehicleImageId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
