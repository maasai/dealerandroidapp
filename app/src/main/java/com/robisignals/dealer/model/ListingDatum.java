package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class ListingDatum {
    @SerializedName("listing_id")
    @Expose
    private String listingId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("vin_number")
    @Expose
    private String vinNumber;
    @SerializedName("engine_size")
    @Expose
    private String engineSize;
    @SerializedName("doors")
    @Expose
    private String doors;
    @SerializedName("exterior_color_id")
    @Expose
    private String extColorId;
    @SerializedName("interior_color_id")
    @Expose
    private String intColorId;
    @SerializedName("condition_id")
    @Expose
    private String conditionId;

    @SerializedName("extra_details")
    @Expose
    private String extraDetails;

    @SerializedName("featured")
    @Expose
    private String featured;

    @SerializedName("fuel_id")
    @Expose
    private String fuelId;

    @SerializedName("make_id")
    @Expose
    private String makeId;

    @SerializedName("mileage")
    @Expose
    private String mileage;

    @SerializedName("model_id")
    @Expose
    private String modelId;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("registration_year")
    @Expose
    private String registrationYear;

    @SerializedName("transmission_id")
    @Expose
    private String transmissionId;

    @SerializedName("active")
    @Expose
    private String active;

    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getConditionId() {
        return conditionId;
    }

    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    public String getExtraDetails() {
        return extraDetails;
    }

    public void setExtraDetails(String extraDetails) {
        this.extraDetails = extraDetails;
    }

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public String getFuelId() {
        return fuelId;
    }

    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRegistrationYear() {
        return registrationYear;
    }

    public void setRegistrationYear(String registrationYear) {
        this.registrationYear = registrationYear;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(String engineSize) {
        this.engineSize = engineSize;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getExtColorId() {
        return extColorId;
    }

    public void setExtColorId(String extColorId) {
        this.extColorId = extColorId;
    }

    public String getIntColorId() {
        return intColorId;
    }

    public void setIntColorId(String intColorId) {
        this.intColorId = intColorId;
    }

    public String getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(String transmissionId) {
        this.transmissionId = transmissionId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }
}
