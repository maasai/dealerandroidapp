package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class FeatureDatum {
    @SerializedName("feature_id")
    @Expose
    private String featureId;
    @SerializedName("feature_name")
    @Expose
    private String featureName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The featureId
     */
    public String getFeatureId() {
        return featureId;
    }

    /**
     *
     * @param featureId
     * The feature_id
     */
    public void setFeatureId(String featureId) {
        this.featureId = featureId;
    }

    /**
     *
     * @return
     * The featureName
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     *
     * @param featureName
     * The feature_name
     */
    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
