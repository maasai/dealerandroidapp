package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class CompanyDatum {
    @SerializedName("setting_id")
    @Expose
    private String companyId;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_address")
    @Expose
    private String companyAddress;
    @SerializedName("company_telephone")
    @Expose
    private String companyPhone;
    @SerializedName("company_email")
    @Expose
    private String companyEmail;
    @SerializedName("company_description")
    @Expose
    private String companyDescription;
    @SerializedName("company_city")
    @Expose
    private String companyCity;
    @SerializedName("company_state")
    @Expose
    private String companyState;
    @SerializedName("company_postal_code")
    @Expose
    private String companyPostalCode;
    @SerializedName("company_currency")
    @Expose
    private String companyCurrency;
    @SerializedName("company_mileage")
    @Expose
    private String companyMileage;
    @SerializedName("company_longitude")
    @Expose
    private String companyLongitude;
    @SerializedName("company_latitude")
    @Expose
    private String companyLatitude;
    @SerializedName("company_facebook")
    @Expose
    private String companyFacebook;
    @SerializedName("company_twitter")
    @Expose
    private String companyTwitter;
    @SerializedName("company_website")
    @Expose
    private String companyWebsite;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }

    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getCompanyTwitter() {
        return companyTwitter;
    }

    public void setCompanyTwitter(String companyTwitter) {
        this.companyTwitter = companyTwitter;
    }

    public String getCompanyFacebook() {
        return companyFacebook;
    }

    public void setCompanyFacebook(String companyFacebook) {
        this.companyFacebook = companyFacebook;
    }

    public String getCompanyLatitude() {
        return companyLatitude;
    }

    public void setCompanyLatitude(String companyLatitude) {
        this.companyLatitude = companyLatitude;
    }

    public String getCompanyLongitude() {
        return companyLongitude;
    }

    public void setCompanyLongitude(String companyLongitude) {
        this.companyLongitude = companyLongitude;
    }

    public String getCompanyMileage() {
        return companyMileage;
    }

    public void setCompanyMileage(String companyMileage) {
        this.companyMileage = companyMileage;
    }

    public String getCompanyCurrency() {
        return companyCurrency;
    }

    public void setCompanyCurrency(String companyCurrency) {
        this.companyCurrency = companyCurrency;
    }

    public String getCompanyPostalCode() {
        return companyPostalCode;
    }

    public void setCompanyPostalCode(String companyPostalCode) {
        this.companyPostalCode = companyPostalCode;
    }

    public String getCompanyState() {
        return companyState;
    }

    public void setCompanyState(String companyState) {
        this.companyState = companyState;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }
}
