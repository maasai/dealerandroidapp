package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class ColorDatum {
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("color_name")
    @Expose
    private String colorName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The colorId
     */
    public String getColorId() {
        return colorId;
    }

    /**
     *
     * @param colorId
     * The color_id
     */
    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    /**
     *
     * @return
     * The colorName
     */
    public String getColorName() {
        return colorName;
    }

    /**
     *
     * @param colorName
     * The color_name
     */
    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
