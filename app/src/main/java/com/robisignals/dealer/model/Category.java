package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class header comment!
 */
public class Category {
    @Expose
    private List<CategoryDatum> data = new ArrayList<CategoryDatum>();
    @Expose
    private Integer timestamp;
    @Expose
    private List<String> desync = new ArrayList<String>();
    @Expose
    private Paginator paginator;

    /**
     *
     * @return
     * The data
     */
    public List<CategoryDatum> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<CategoryDatum> data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public Integer getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     * The desync
     */
    public List<String> getDesync() {
        return desync;
    }

    /**
     *
     * @param desync
     * The desync
     */
    public void setDesync(List<String> desync) {
        this.desync = desync;
    }

    /**
     *
     * @return
     * The paginator
     */
    public Paginator getPaginator() {
        return paginator;
    }

    /**
     *
     * @param paginator
     * The paginator
     */
    public void setPaginator(Paginator paginator) {
        this.paginator = paginator;
    }
}
