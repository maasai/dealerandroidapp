package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class TransmissionDatum {
    @SerializedName("transmission_id")
    @Expose
    private String transmissionId;
    @SerializedName("transmission_name")
    @Expose
    private String transmissionName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The transmissionId
     */
    public String getTransmissionId() {
        return transmissionId;
    }

    /**
     *
     * @param transmissionId
     * The transmission_id
     */
    public void setTransmissionId(String transmissionId) {
        this.transmissionId = transmissionId;
    }

    /**
     *
     * @return
     * The transmissionName
     */
    public String getTransmissionName() {
        return transmissionName;
    }

    /**
     *
     * @param transmissionName
     * The transmission_name
     */
    public void setTransmissionName(String transmissionName) {
        this.transmissionName = transmissionName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
