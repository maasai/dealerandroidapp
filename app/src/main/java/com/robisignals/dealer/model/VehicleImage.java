package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class header comment!
 */
public class VehicleImage {

    @Expose
    private List<VehicleImageDatum> data = new ArrayList<VehicleImageDatum>();
    @Expose
    private Integer timestamp;
    @Expose
    private List<String> desync = new ArrayList<String>();
    @Expose
    private Paginator paginator;

    public List<VehicleImageDatum> getData() {
        return data;
    }

    public void setData(List<VehicleImageDatum> data) {
        this.data = data;
    }

    public Paginator getPaginator() {
        return paginator;
    }

    public void setPaginator(Paginator paginator) {
        this.paginator = paginator;
    }

    public List<String> getDesync() {
        return desync;
    }

    public void setDesync(List<String> desync) {
        this.desync = desync;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }
}
