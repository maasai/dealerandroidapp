package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class MakeDatum {
    @SerializedName("make_id")
    @Expose
    private String makeId;
    @SerializedName("make_name")
    @Expose
    private String makeName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The makeId
     */
    public String getMakeId() {
        return makeId;
    }

    /**
     *
     * @param makeId
     * The make_id
     */
    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    /**
     *
     * @return
     * The makeName
     */
    public String getMakeName() {
        return makeName;
    }

    /**
     *
     * @param makeName
     * The make_name
     */
    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
