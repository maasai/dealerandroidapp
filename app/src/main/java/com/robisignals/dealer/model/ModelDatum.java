package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class ModelDatum {
    @SerializedName("model_id")
    @Expose
    private String modelId;
    @SerializedName("make_id")
    @Expose
    private String makeId;
    @SerializedName("model_name")
    @Expose
    private String modelName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The modelId
     */
    public String getModelId() {
        return modelId;
    }

    /**
     *
     * @param modelId
     * The model_id
     */
    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    /**
     *
     * @return
     * The modelName
     */
    public String getModelName() {
        return modelName;
    }

    /**
     *
     * @param modelName
     * The model_name
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
