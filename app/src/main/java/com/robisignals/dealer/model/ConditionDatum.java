package com.robisignals.dealer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * TODO: Add a class header comment!
 */
public class ConditionDatum {
    @SerializedName("condition_id")
    @Expose
    private String conditionId;
    @SerializedName("condition_name")
    @Expose
    private String conditionName;
    @SerializedName("created_at")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("updated_at")
    @Expose
    private UpdatedAt updatedAt;

    /**
     *
     * @return
     * The conditionId
     */
    public String getConditionId() {
        return conditionId;
    }

    /**
     *
     * @param conditionId
     * The condition_id
     */
    public void setConditionId(String conditionId) {
        this.conditionId = conditionId;
    }

    /**
     *
     * @return
     * The conditionName
     */
    public String getConditionName() {
        return conditionName;
    }

    /**
     *
     * @param conditionName
     * The condition_name
     */
    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return
     * The updatedAt
     */
    public UpdatedAt getUpdatedAt() {
        return updatedAt;
    }

    /**
     *
     * @param updatedAt
     * The updated_at
     */
    public void setUpdatedAt(UpdatedAt updatedAt) {
        this.updatedAt = updatedAt;
    }
}
