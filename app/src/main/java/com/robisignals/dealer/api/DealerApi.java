package com.robisignals.dealer.api;

import com.robisignals.dealer.model.Category;
import com.robisignals.dealer.model.Color;
import com.robisignals.dealer.model.Company;
import com.robisignals.dealer.model.Condition;
import com.robisignals.dealer.model.Feature;
import com.robisignals.dealer.model.FeatureListing;
import com.robisignals.dealer.model.Fuel;
import com.robisignals.dealer.model.Listing;
import com.robisignals.dealer.model.Make;
import com.robisignals.dealer.model.Model;
import com.robisignals.dealer.model.Transmission;
import com.robisignals.dealer.model.VehicleImage;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * TODO: Add a class header comment!
 */
public interface DealerApi {

    @GET("/features")
    public Feature getFeatures(@QueryMap Map<String, Integer> options);

    @GET("/fuels")
    public Fuel getFuels(@QueryMap Map<String, Integer> options);

    @GET("/colors")
    public Color getColors(@QueryMap Map<String, Integer> options);

    @GET("/settings")
    public Company getCompanies(@QueryMap Map<String, Integer> options);

    @GET("/conditions")
    public Condition getConditions(@QueryMap Map<String, Integer> options);

    @GET("/categories")
    public Category getCategories(@QueryMap Map<String, Integer> options);

    @GET("/transmissions")
    public Transmission getTransmissions(@QueryMap Map<String, Integer> options);

    @GET("/makes")
    public Make getMakes(@QueryMap Map<String, Integer> options);

    @GET("/models")
    public Model getModels(@QueryMap Map<String, Integer> options);

    @GET("/listings")
    public Listing getListings(@QueryMap Map<String, Integer> options);

    @GET("/feature_listings")
    public FeatureListing getFeatureListings(@QueryMap Map<String, Integer> options);

    @GET("/vehicle_images")
    public VehicleImage getVehicleImages(@QueryMap Map<String, Integer> options);


}
