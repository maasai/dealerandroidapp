package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.Model;
import com.robisignals.dealer.model.ModelDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class ModelsSyncHelper {
    public static final String LOG_TAG = ModelsSyncHelper.class.getSimpleName();
    private static final String MODELS_LAST_SYNC = "models_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync models local table with remote db
     * @param context
     * @param account
     */
    public static void syncModels(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, MODELS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the models
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Model model = dealer.getModels(params);

            int totalPages = model.getPaginator().getTotalPages();
            int currentPage = model.getPaginator().getCurrentPage();
            int time = model.getTimestamp();

            List<ModelDatum> models = model.getData();

            //save models in db here
            processModel(context, models);

            //There are some ids to delete
            if (model.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < model.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.ModelsEntry.CONTENT_URI)
                            .withSelection(DealerContract.ModelsEntry.MODEL_ID + " = ?", new String[]{model.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + model.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }

            Log.d(LOG_TAG, "The current page is: " + currentPage);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Model modelNext = dealer.getModels(params);
                    processModel(context, modelNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + modelNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + modelNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }
            }

            Log.d(LOG_TAG, "The timestamp is: " + time);

            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, MODELS_LAST_SYNC, String.valueOf(time));
            }
        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processModel(Context context, List<ModelDatum> models){
        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < models.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = models.get(i).getCreatedAt();
            UpdatedAt updatedAt = models.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String model_id = models.get(i).getModelId();
            String make_id = models.get(i).getMakeId();
            String model_name = models.get(i).getModelName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            ContentValues modelValues = new ContentValues();

            modelValues.put(DealerContract.ModelsEntry.MODEL_ID, model_id);
            modelValues.put(DealerContract.ModelsEntry.MAKE_ID, make_id);
            modelValues.put(DealerContract.ModelsEntry.MODEL_NAME, model_name);
            modelValues.put(DealerContract.ModelsEntry.CREATED_AT, created_at);
            arrayList.add(modelValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.ModelsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }

    }
}
