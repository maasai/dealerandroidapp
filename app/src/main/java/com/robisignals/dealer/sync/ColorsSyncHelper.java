package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.Color;
import com.robisignals.dealer.model.ColorDatum;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class ColorsSyncHelper {
    public static final String LOG_TAG = ColorsSyncHelper.class.getSimpleName();
    private static final String COLORS_LAST_SYNC = "colors_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);

    /**
     * Sync colors local table with remote db
     * @param context
     * @param account
     */
    public static void syncColors(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, COLORS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);
     try {
         Map<String, Integer> params = new HashMap<>();
         params.put("modifiedSince", lastUpdate);
        // params.put("modifiedSince", 0);

         Color color = dealer.getColors(params);

        int totalPages = color.getPaginator().getTotalPages();
        int currentPage = color.getPaginator().getCurrentPage();
        int time = color.getTimestamp();

         List<ColorDatum> colors = color.getData();

         //save colors in db here
         processColor(context, colors);

         //There are some ids to delete
         //Assumed that all ids to delete will be in the first page of our paginated data
         if (color.getDesync().size() > 0) {
             ArrayList<ContentProviderOperation> operations = new ArrayList<>();
             ContentProviderOperation operation;
             for (int i = 0; i < color.getDesync().size(); i++) {
                 operation = ContentProviderOperation
                         .newDelete(DealerContract.ColorsEntry.CONTENT_URI)
                         .withSelection(DealerContract.ColorsEntry.COLOR_ID + " = ?", new String[]{color.getDesync().get(i)})
                         .build();
                 operations.add(operation);
                 Log.d(LOG_TAG, "Added uuid for delete: " + color.getDesync().get(i));
             }
             try {
                 context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
             } catch (RemoteException e) {
                 Log.i(LOG_TAG, "There was an error: " + e.toString());

             } catch (OperationApplicationException e) {
                 Log.i(LOG_TAG, "There was an error: " + e.toString());
             }
         }

         Log.d(LOG_TAG, "The current page is: " + currentPage);

         //Loop through the paginated data
         if (totalPages > 1){
             for (int i = currentPage; i != totalPages; i++){
                 params.put("page", i+1);

                 Color colorNext = dealer.getColors(params);
                 processColor(context, colorNext.getData());

                 Log.d(LOG_TAG, "The current page is: " + colorNext.getPaginator().getCurrentPage());
                 Log.d(LOG_TAG, "The total number of page is : " + colorNext.getPaginator().getTotalPages());
                 Log.d(LOG_TAG, "The timestamp is: " + time);
             }

         }
         //save timestamp in the active sync account. Timestamp for the first loaded page is used.
         if (0 < time) {
             mgr.setUserData(account, COLORS_LAST_SYNC, String.valueOf(time));
         }

     }catch (Exception e){
             Log.e(LOG_TAG, "There was an error: "+ e.toString());
         }
    }

    /**
     * Do db operations
     */
    private static void processColor(Context context, List<ColorDatum> colors){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        for (int i = 0; i < colors.size(); i++) {
            //These two have own implementation.
            CreatedAt createdAt = colors.get(i).getCreatedAt();
            UpdatedAt updatedAt = colors.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String color_id = colors.get(i).getColorId();
            String color_name = colors.get(i).getColorName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Color ID: " + color_id + " Color name: " + color_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues colorValues = new ContentValues();

            colorValues.put(DealerContract.ColorsEntry.COLOR_ID, color_id);
            colorValues.put(DealerContract.ColorsEntry.COLOR_NAME, color_name);
            colorValues.put(DealerContract.ColorsEntry.CREATED_AT, created_at);
            arrayList.add(colorValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.ColorsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
