package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.Category;
import com.robisignals.dealer.model.CategoryDatum;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class CategoriesSyncHelper {
    public static final String LOG_TAG = CategoriesSyncHelper.class.getSimpleName();
    private static final String CATEGORIES_LAST_SYNC = "categories_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync categories local table with remote db
     * @param context
     * @param account
     */
    public static void syncCategories(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, CATEGORIES_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the categories
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Category category = dealer.getCategories(params);


            int totalPages = category.getPaginator().getTotalPages();
            int currentPage = category.getPaginator().getCurrentPage();
            int time = category.getTimestamp();

            List<CategoryDatum> categories = category.getData();

            //save categories in db here
            processCategory(context, categories);

            //There are some ids to delete
            if (category.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < category.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.CategoriesEntry.CONTENT_URI)
                            .withSelection(DealerContract.CategoriesEntry.CATEGORY_ID + " = ?", new String[]{category.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + category.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }

            Log.d(LOG_TAG, "The current page is: " + currentPage);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Category categoryNext = dealer.getCategories(params);
                    processCategory(context, categoryNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + categoryNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + categoryNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }

            Log.d(LOG_TAG, "The timestamp is: " + time);

            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, CATEGORIES_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processCategory(Context context, List<CategoryDatum> categories){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < categories.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = categories.get(i).getCreatedAt();
            UpdatedAt updatedAt = categories.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String category_id = categories.get(i).getCategoryId();
            String category_name = categories.get(i).getCategoryName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Category ID: " + category_id + " Category name: " + category_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues categoryValues = new ContentValues();

            categoryValues.put(DealerContract.CategoriesEntry.CATEGORY_ID, category_id);
            categoryValues.put(DealerContract.CategoriesEntry.CATEGORY_NAME, category_name);
            categoryValues.put(DealerContract.CategoriesEntry.CREATED_AT, created_at);
            arrayList.add(categoryValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.CategoriesEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
