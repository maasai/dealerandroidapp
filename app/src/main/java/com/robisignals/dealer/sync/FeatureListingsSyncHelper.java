package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.FeatureListing;
import com.robisignals.dealer.model.FeatureListingDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class FeatureListingsSyncHelper {
    public static final String LOG_TAG = FeatureListingsSyncHelper.class.getSimpleName();
    private static final String FEATURE_LISTINGS_LAST_SYNC = "feature_listings_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync featureListings local table with remote db
     * @param context
     * @param account
     */
    public static void syncFeatureListings(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, FEATURE_LISTINGS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the featureListings
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            FeatureListing featureListing = dealer.getFeatureListings(params);

            int totalPages = featureListing.getPaginator().getTotalPages();
            int currentPage = featureListing.getPaginator().getCurrentPage();
            int time = featureListing.getTimestamp();

            List<FeatureListingDatum> featureListings = featureListing.getData();

            //save featureListings in db here
            processData(context, featureListings);

            //There are some ids to delete
            if (featureListing.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < featureListing.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.FeatureListingsEntry.CONTENT_URI)
                            .withSelection(DealerContract.FeatureListingsEntry.FEATURE_LISTING_ID + " = ?", new String[]{featureListing.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + featureListing.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }

            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    FeatureListing featureListingNext = dealer.getFeatureListings(params);
                    processData(context, featureListingNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + featureListingNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + featureListingNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }

            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, FEATURE_LISTINGS_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processData(Context context, List<FeatureListingDatum> featureListings){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < featureListings.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = featureListings.get(i).getCreatedAt();
            UpdatedAt updatedAt = featureListings.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String feature_listing_id = featureListings.get(i).getFeatureListingId();
            String feature_id = featureListings.get(i).getFeatureId();
            String listing_id = featureListings.get(i).getListingId();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            ContentValues featureListingValues = new ContentValues();

            featureListingValues.put(DealerContract.FeatureListingsEntry.FEATURE_LISTING_ID, feature_listing_id);
            featureListingValues.put(DealerContract.FeatureListingsEntry.FEATURE_ID, feature_id);
            featureListingValues.put(DealerContract.FeatureListingsEntry.LISTING_ID, listing_id);
            featureListingValues.put(DealerContract.FeatureListingsEntry.CREATED_AT, created_at);
            arrayList.add(featureListingValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.FeatureListingsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
