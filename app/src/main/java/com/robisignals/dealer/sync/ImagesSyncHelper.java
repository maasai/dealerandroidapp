package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.model.VehicleImage;
import com.robisignals.dealer.model.VehicleImageDatum;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class ImagesSyncHelper {
    public static final String LOG_TAG = ImagesSyncHelper.class.getSimpleName();
    private static final String IMAGES_LAST_SYNC = "images_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync images local table with remote db
     * @param context
     * @param account
     */
    public static void syncImages(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, IMAGES_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the images
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            VehicleImage image = dealer.getVehicleImages(params);

            int totalPages = image.getPaginator().getTotalPages();
            int currentPage = image.getPaginator().getCurrentPage();
            int time = image.getTimestamp();

            List<VehicleImageDatum> images = image.getData();

            //save images in db here
            processData(context, images);

            //There are some ids to delete
            if (image.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < image.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.VehicleImagesEntry.CONTENT_URI)
                            .withSelection(DealerContract.VehicleImagesEntry.VEHICLE_IMAGE_ID + " = ?", new String[]{image.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + image.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }


            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    VehicleImage imageNext = dealer.getVehicleImages(params);
                    processData(context, imageNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + imageNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + imageNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }

            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, IMAGES_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processData(Context context, List<VehicleImageDatum> images){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < images.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = images.get(i).getCreatedAt();
            UpdatedAt updatedAt = images.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String vehicle_image_id = images.get(i).getVehicleImageId();
            String filename = images.get(i).getFilename();
            String listing_id = images.get(i).getListingId();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n image ID: " + vehicle_image_id + " image filename: " + filename + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues imageValues = new ContentValues();

            imageValues.put(DealerContract.VehicleImagesEntry.VEHICLE_IMAGE_ID, vehicle_image_id);
            imageValues.put(DealerContract.VehicleImagesEntry.FILE_NAME, filename);
            imageValues.put(DealerContract.VehicleImagesEntry.LISTING_ID, listing_id);
            imageValues.put(DealerContract.VehicleImagesEntry.CREATED_AT, created_at);
            arrayList.add(imageValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.VehicleImagesEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
