package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.Make;
import com.robisignals.dealer.model.MakeDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class MakesSyncHelper {
    public static final String LOG_TAG = MakesSyncHelper.class.getSimpleName();
    private static final String MAKES_LAST_SYNC = "makes_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync makes local table with remote db
     * @param context
     * @param account
     */
    public static void syncMakes(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, MAKES_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the makes
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Make make = dealer.getMakes(params);

            int totalPages = make.getPaginator().getTotalPages();
            int currentPage = make.getPaginator().getCurrentPage();
            int time = make.getTimestamp();

            List<MakeDatum> makes = make.getData();

            //save makes in db here
            processData(context, makes);

            //There are some ids to delete
            if (make.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < make.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.MakesEntry.CONTENT_URI)
                            .withSelection(DealerContract.MakesEntry.MAKE_ID + " = ?", new String[]{make.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + make.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }

            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Make makeNext = dealer.getMakes(params);
                    processData(context, makeNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + makeNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + makeNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, MAKES_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processData(Context context, List<MakeDatum> makes){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < makes.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = makes.get(i).getCreatedAt();
            UpdatedAt updatedAt = makes.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String make_id = makes.get(i).getMakeId();
            String make_name = makes.get(i).getMakeName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Make ID: " + make_id + " Make name: " + make_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues makeValues = new ContentValues();

            makeValues.put(DealerContract.MakesEntry.MAKE_ID, make_id);
            makeValues.put(DealerContract.MakesEntry.MAKE_NAME, make_name);
            makeValues.put(DealerContract.MakesEntry.CREATED_AT, created_at);
            arrayList.add(makeValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.MakesEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
