package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.Transmission;
import com.robisignals.dealer.model.TransmissionDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class TransmissionsSyncHelper {
    public static final String LOG_TAG = TransmissionsSyncHelper.class.getSimpleName();
    private static final String TRANSMISSIONS_LAST_SYNC = "transmissions_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);

    /**
     * Sync transmissions local table with remote db
     * @param context
     * @param account
     */
    public static void syncTransmissions(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, TRANSMISSIONS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the transmissions
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Transmission transmission = dealer.getTransmissions(params);

            int totalPages = transmission.getPaginator().getTotalPages();
            int currentPage = transmission.getPaginator().getCurrentPage();
            int time = transmission.getTimestamp();

            List<TransmissionDatum> transmissions = transmission.getData();
            //save transmissions in db here
            processData(context, transmissions);

            //There are some ids to delete
            if (transmission.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < transmission.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.TransmissionsEntry.CONTENT_URI)
                            .withSelection(DealerContract.TransmissionsEntry.TRANSMISSION_ID + " = ?", new String[]{transmission.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + transmission.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }

            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Transmission transmissionNext = dealer.getTransmissions(params);
                    processData(context, transmissionNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + transmissionNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + transmissionNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, TRANSMISSIONS_LAST_SYNC, String.valueOf(time));
            }

        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processData(Context context, List<TransmissionDatum> transmissions){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < transmissions.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = transmissions.get(i).getCreatedAt();
            UpdatedAt updatedAt = transmissions.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String transmission_id = transmissions.get(i).getTransmissionId();
            String transmission_name = transmissions.get(i).getTransmissionName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Transmission ID: " + transmission_id + " Transmission name: " + transmission_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues transmissionValues = new ContentValues();

            transmissionValues.put(DealerContract.TransmissionsEntry.TRANSMISSION_ID, transmission_id);
            transmissionValues.put(DealerContract.TransmissionsEntry.TRANSMISSION_NAME, transmission_name);
            transmissionValues.put(DealerContract.TransmissionsEntry.CREATED_AT, created_at);
            arrayList.add(transmissionValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.TransmissionsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
