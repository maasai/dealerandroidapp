package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.Company;
import com.robisignals.dealer.model.CompanyDatum;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class SettingsSyncHelper {
    public static final String LOG_TAG = SettingsSyncHelper.class.getSimpleName();
    private static final String COMPANIES_LAST_SYNC = "companies_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);

    /**
     * Sync settings local table with remote db
     * @param context
     * @param account
     */
    public static void sync(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, COMPANIES_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the companies
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Company company = dealer.getCompanies(params);

            int totalPages = company.getPaginator().getTotalPages();
            int currentPage = company.getPaginator().getCurrentPage();
            int time = company.getTimestamp();

            List<CompanyDatum> companies = company.getData();

            //save company settings in db here
            processCompany(context, companies);


            //There are some ids to delete
            if (company.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < company.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.SettingsEntry.CONTENT_URI)
                            .withSelection(DealerContract.SettingsEntry.SETTING_ID + " = ?", new String[]{company.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + company.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }
            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Company companyNext = dealer.getCompanies(params);
                    processCompany(context, companyNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + companyNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + companyNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, COMPANIES_LAST_SYNC, String.valueOf(time));
            }

        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processCompany(Context context, List<CompanyDatum> companies){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < companies.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = companies.get(i).getCreatedAt();
            UpdatedAt updatedAt = companies.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String company_id = companies.get(i).getCompanyId();
            String company_name = companies.get(i).getCompanyName();
            String company_address = companies.get(i).getCompanyAddress();
            String company_phone = companies.get(i).getCompanyPhone();
            String company_email = companies.get(i).getCompanyEmail();
            String company_description = companies.get(i).getCompanyDescription();
            String company_city = companies.get(i).getCompanyCity();
            String company_state = companies.get(i).getCompanyState();
            String company_postal_code = companies.get(i).getCompanyPostalCode();
            String company_currency = companies.get(i).getCompanyCurrency();
            String company_mileage = companies.get(i).getCompanyMileage();
            String company_longitude = companies.get(i).getCompanyLongitude();
            String company_latitude = companies.get(i).getCompanyLatitude();
            String company_facebook = companies.get(i).getCompanyFacebook();
            String company_twitter = companies.get(i).getCompanyTwitter();
            String company_website = companies.get(i).getCompanyWebsite();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            ContentValues companyValues = new ContentValues();

            companyValues.put(DealerContract.SettingsEntry.SETTING_ID, company_id);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_NAME, company_name);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_ADDRESS, company_address);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_PHONE, company_phone);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_EMAIL, company_email);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_DESCRIPTION, company_description);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_CITY, company_city);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_STATE, company_state);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_POSTAL_CODE, company_postal_code);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_CURRENCY, company_currency);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_MILEAGE_UNITS, company_mileage);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_LONGITUDE, company_longitude);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_LATITUDE, company_latitude);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_FACEBOOK, company_facebook);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_TWITTER, company_twitter);
            companyValues.put(DealerContract.SettingsEntry.COMPANY_WEBSITE, company_website);
            companyValues.put(DealerContract.SettingsEntry.CREATED_AT, created_at);
            arrayList.add(companyValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.SettingsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
