package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.Feature;
import com.robisignals.dealer.model.FeatureDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class FeaturesSyncHelper {
    public static final String LOG_TAG = FeaturesSyncHelper.class.getSimpleName();
    private static final String FEATURES_LAST_SYNC = "features_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync features local table with remote db
     * @param context
     * @param account
     */
    public static void syncFeatures(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, FEATURES_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the features
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Feature feature = dealer.getFeatures(params);

            int totalPages = feature.getPaginator().getTotalPages();
            int currentPage = feature.getPaginator().getCurrentPage();
            int time = feature.getTimestamp();

            List<FeatureDatum> features = feature.getData();

            //save features in db here
            processFeature(context, features);

            //There are some ids to delete
            if (feature.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < feature.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.FeaturesEntry.CONTENT_URI)
                            .withSelection(DealerContract.FeaturesEntry.FEATURE_ID + " = ?", new String[]{feature.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + feature.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }


            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Feature featureNext = dealer.getFeatures(params);
                    processFeature(context, featureNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + featureNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + featureNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, FEATURES_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processFeature(Context context, List<FeatureDatum> features){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < features.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = features.get(i).getCreatedAt();
            UpdatedAt updatedAt = features.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String feature_id = features.get(i).getFeatureId();
            String feature_name = features.get(i).getFeatureName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Feature ID: " + feature_id + " Feature name: " + feature_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues featureValues = new ContentValues();

            featureValues.put(DealerContract.FeaturesEntry.FEATURE_ID, feature_id);
            featureValues.put(DealerContract.FeaturesEntry.FEATURE_NAME, feature_name);
            featureValues.put(DealerContract.FeaturesEntry.CREATED_AT, created_at);
            arrayList.add(featureValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.FeaturesEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
