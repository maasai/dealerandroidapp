package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.Listing;
import com.robisignals.dealer.model.ListingDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class ListingsSyncHelper {
    public static final String LOG_TAG = ListingsSyncHelper.class.getSimpleName();
    private static final String LISTINGS_LAST_SYNC = "listings_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync listings local table with remote db
     * @param context
     * @param account
     */
    public static void syncListings(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, LISTINGS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the listings
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Listing listing = dealer.getListings(params);

            int totalPages = listing.getPaginator().getTotalPages();
            int currentPage = listing.getPaginator().getCurrentPage();
            int time = listing.getTimestamp();

            List<ListingDatum> listings = listing.getData();



            //There are some ids to delete
            if (listing.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < listing.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.ListingsEntry.CONTENT_URI)
                            .withSelection(DealerContract.ListingsEntry.LISTING_ID + " = ?", new String[]{listing.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + listing.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }

            //save listings in db here
            processData(context, listings);

            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Listing listingNext = dealer.getListings(params);
                    processData(context, listingNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + listingNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + listingNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, LISTINGS_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processData(Context context, List<ListingDatum> listings){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < listings.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = listings.get(i).getCreatedAt();
            UpdatedAt updatedAt = listings.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String listing_id = listings.get(i).getListingId();
            String category_id = listings.get(i).getCategoryId();
            String ext_color_id = listings.get(i).getExtColorId();
            String int_color_id = listings.get(i).getIntColorId();
            String doors = listings.get(i).getDoors();
            String engine_size = listings.get(i).getEngineSize();
            String vin_number = listings.get(i).getVinNumber();
            String condition_id = listings.get(i).getConditionId();
            String extra_details = listings.get(i).getExtraDetails();
            String featured = listings.get(i).getFeatured();
            String fuel_id = listings.get(i).getFuelId();
            String make_id = listings.get(i).getMakeId();
            String mileage = listings.get(i).getMileage();
            String model_id = listings.get(i).getModelId();
            String price = listings.get(i).getPrice();
            String registration_year = listings.get(i).getRegistrationYear();
            String transmission_id = listings.get(i).getTransmissionId();
            String active = listings.get(i).getActive();

            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            ContentValues listingValues = new ContentValues();

            listingValues.put(DealerContract.ListingsEntry.LISTING_ID, listing_id);
            listingValues.put(DealerContract.ListingsEntry.CATEGORY_ID, category_id);
            listingValues.put(DealerContract.ListingsEntry.INT_COLOR_ID, int_color_id);
            listingValues.put(DealerContract.ListingsEntry.EXT_COLOR_ID, ext_color_id);
            listingValues.put(DealerContract.ListingsEntry.DOORS, doors);
            listingValues.put(DealerContract.ListingsEntry.VIN_NUMBER, vin_number);
            listingValues.put(DealerContract.ListingsEntry.ENGINE_SIZE, engine_size);
            listingValues.put(DealerContract.ListingsEntry.CONDITION_ID, condition_id);
            listingValues.put(DealerContract.ListingsEntry.EXTRA_DETAILS, extra_details);
            listingValues.put(DealerContract.ListingsEntry.FEATURED, featured);
            listingValues.put(DealerContract.ListingsEntry.FUEL_ID, fuel_id);
            listingValues.put(DealerContract.ListingsEntry.MAKE_ID, make_id);
            listingValues.put(DealerContract.ListingsEntry.MILEAGE, mileage);
            listingValues.put(DealerContract.ListingsEntry.MODEL_ID, model_id);
            listingValues.put(DealerContract.ListingsEntry.PRICE, price);
            listingValues.put(DealerContract.ListingsEntry.REGISTRATION_YEAR, registration_year);
            listingValues.put(DealerContract.ListingsEntry.TRANSMISSION_ID, transmission_id);
            listingValues.put(DealerContract.ListingsEntry.ACTIVE, active);
            listingValues.put(DealerContract.ListingsEntry.CREATED_AT, created_at);
            arrayList.add(listingValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.ListingsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
