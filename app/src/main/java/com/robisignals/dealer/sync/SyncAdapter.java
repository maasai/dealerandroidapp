package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

/**
 * TODO: Add a class header comment!
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String LOG_TAG  = SyncAdapter.class.getSimpleName();
    public static final String BASE_URL = "http://dealer.twigahost.com";
    private final ContentResolver mContentResolver;
    Context mContext;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
        mContext = context;
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d("Dealer", "onPerformSync for account[" + account.name + "]");
        Log.i(LOG_TAG, "Started data sync ...");

        //sync vehicle images
        ImagesSyncHelper.syncImages(getContext(), account);

        //sync makes
        MakesSyncHelper.syncMakes(getContext(), account);

        //sync models
        ModelsSyncHelper.syncModels(getContext(), account);

        //sync listings
        ListingsSyncHelper.syncListings(getContext(), account);

        //sync feature_listings
        FeatureListingsSyncHelper.syncFeatureListings(getContext(), account);

        //sync fuels
        FuelsSyncHelper.syncFuels(getContext(), account);

        //sync transmissions
        TransmissionsSyncHelper.syncTransmissions(getContext(), account);

        //sync conditions
        ConditionsSyncHelper.syncConditions(getContext(), account);

        //sync features
        FeaturesSyncHelper.syncFeatures(getContext(), account);

        //DO sync for colors table
        ColorsSyncHelper.syncColors(getContext(), account);

        //sync Categories
        CategoriesSyncHelper.syncCategories(getContext(), account);

        //sync companies
        SettingsSyncHelper.sync(getContext(), account);

    }

}