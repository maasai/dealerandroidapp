package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.Fuel;
import com.robisignals.dealer.model.FuelDatum;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class FuelsSyncHelper {
    public static final String LOG_TAG = FuelsSyncHelper.class.getSimpleName();
    private static final String FUELS_LAST_SYNC = "fuels_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync fuels local table with remote db
     * @param context
     * @param account
     */
    public static void syncFuels(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, FUELS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the fuels
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Fuel fuel = dealer.getFuels(params);

            int totalPages = fuel.getPaginator().getTotalPages();
            int currentPage = fuel.getPaginator().getCurrentPage();
            int time = fuel.getTimestamp();

            List<FuelDatum> fuels = fuel.getData();

            //save fuels in db here
            processFuel(context, fuels);

            //There are some ids to delete
            if (fuel.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < fuel.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.FuelsEntry.CONTENT_URI)
                            .withSelection(DealerContract.FuelsEntry.FUEL_ID + " = ?", new String[]{fuel.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + fuel.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }


            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Fuel fuelNext = dealer.getFuels(params);
                    processFuel(context, fuelNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + fuelNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + fuelNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, FUELS_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processFuel(Context context, List<FuelDatum> fuels){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < fuels.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = fuels.get(i).getCreatedAt();
            UpdatedAt updatedAt = fuels.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String fuel_id = fuels.get(i).getFuelId();
            String fuel_name = fuels.get(i).getFuelName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Fuel ID: " + fuel_id + " Fuel name: " + fuel_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues fuelValues = new ContentValues();

            fuelValues.put(DealerContract.FuelsEntry.FUEL_ID, fuel_id);
            fuelValues.put(DealerContract.FuelsEntry.FUEL_NAME, fuel_name);
            fuelValues.put(DealerContract.FuelsEntry.CREATED_AT, created_at);
            arrayList.add(fuelValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.FuelsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }

}
