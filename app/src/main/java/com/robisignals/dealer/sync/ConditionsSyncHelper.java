package com.robisignals.dealer.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;

import com.robisignals.dealer.api.DealerApi;
import com.robisignals.dealer.model.Condition;
import com.robisignals.dealer.model.ConditionDatum;
import com.robisignals.dealer.model.CreatedAt;
import com.robisignals.dealer.model.UpdatedAt;
import com.robisignals.dealer.provider.DealerContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * TODO: Add a class header comment!
 */
public class ConditionsSyncHelper {
    public static final String LOG_TAG = ConditionsSyncHelper.class.getSimpleName();
    private static final String CONDITIONS_LAST_SYNC = "conditions_last_sync";

    private static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(SyncAdapter.BASE_URL).build();
    private static DealerApi dealer  = restAdapter.create(DealerApi.class);


    /**
     * Sync conditions local table with remote db
     * @param context
     * @param account
     */
    public static void syncConditions(Context context, Account account){
        //We need an account so we can handle the last sync time which we store in the active account
        AccountManager mgr  = AccountManager.get(context);

        int lastUpdate = 1432224915; //This means we are shipping the app with some preloaded db. Otherwise this should be zero.
        String ts = mgr.getUserData(account, CONDITIONS_LAST_SYNC);

        if (null != ts) {
            try { lastUpdate = Integer.parseInt(ts); }
            catch (NumberFormatException e) {e.printStackTrace(); }
        }
        Log.d(LOG_TAG, "The last sync was: " + lastUpdate);

        try {
            //Fetch the conditions
            Map<String, Integer> params = new HashMap<>();
            params.put("modifiedSince", lastUpdate);
            Condition condition = dealer.getConditions(params);

            int totalPages = condition.getPaginator().getTotalPages();
            int currentPage = condition.getPaginator().getCurrentPage();
            int time = condition.getTimestamp();
            List<ConditionDatum> conditions = condition.getData();

            //save conditions in db here
            processCondition(context, conditions);

            //There are some ids to delete
            if (condition.getDesync().size() > 0) {
                ArrayList<ContentProviderOperation> operations = new ArrayList<>();
                ContentProviderOperation operation;
                for (int i = 0; i < condition.getDesync().size(); i++) {
                    operation = ContentProviderOperation
                            .newDelete(DealerContract.ConditionsEntry.CONTENT_URI)
                            .withSelection(DealerContract.ConditionsEntry.CONDITION_ID + " = ?", new String[]{condition.getDesync().get(i)})
                            .build();
                    operations.add(operation);
                    Log.d(LOG_TAG, "Added uuid for delete: " + condition.getDesync().get(i));
                }
                try {
                    context.getContentResolver().applyBatch(DealerContract.CONTENT_AUTHORITY, operations);
                } catch (RemoteException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());

                } catch (OperationApplicationException e) {
                    Log.i(LOG_TAG, "There was an error: " + e.toString());
                }
            }


            Log.d(LOG_TAG, "The timestamp is: " + time);

            //Loop through the paginated data
            if (totalPages > 1){
                for (int i = currentPage; i != totalPages; i++){
                    params.put("page", i+1);

                    Condition conditionNext = dealer.getConditions(params);
                    processCondition(context, conditionNext.getData());

                    Log.d(LOG_TAG, "The current page is: " + conditionNext.getPaginator().getCurrentPage());
                    Log.d(LOG_TAG, "The total number of page is : " + conditionNext.getPaginator().getTotalPages());
                    Log.d(LOG_TAG, "The timestamp is: " + time);
                }

            }
            //save timestamp in the active sync account
            if (0 < time) {
                mgr.setUserData(account, CONDITIONS_LAST_SYNC, String.valueOf(time));
            }




        }catch (Exception e){
            Log.e(LOG_TAG, "There was an error: "+ e.toString());
        }
    }

    /**
     * Do db operations
     */
    private static void processCondition(Context context, List<ConditionDatum> conditions){

        ArrayList<ContentValues> arrayList = new ArrayList<>();

        //loop through the array and extract fields
        for (int i = 0; i < conditions.size(); i++) {
            //These two have own classes.
            CreatedAt createdAt = conditions.get(i).getCreatedAt();
            UpdatedAt updatedAt = conditions.get(i).getUpdatedAt();

            //Extract fields ready for local db
            String condition_id = conditions.get(i).getConditionId();
            String condition_name = conditions.get(i).getConditionName();
            String created_at = createdAt.getDate();
            String updated_at = updatedAt.getDate();

            Log.d(LOG_TAG, "Converted a row of data and ready to save locally:\n Condition ID: " + condition_id + " Condition name: " + condition_name + " Created at: " + created_at + " Updated at: " + updated_at);

            ContentValues conditionValues = new ContentValues();

            conditionValues.put(DealerContract.ConditionsEntry.CONDITION_ID, condition_id);
            conditionValues.put(DealerContract.ConditionsEntry.CONDITION_NAME, condition_name);
            conditionValues.put(DealerContract.ConditionsEntry.CREATED_AT, created_at);
            arrayList.add(conditionValues);
        }

        if (arrayList.size() > 0) {
            ContentValues[] contentValues = new ContentValues[arrayList.size()];
            arrayList.toArray(contentValues);

            context.getContentResolver().bulkInsert(DealerContract.ConditionsEntry.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "Added some data in db: ");
        }
    }
}
