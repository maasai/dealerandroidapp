package com.robisignals.dealer.ui;

import android.content.Context;

import com.robisignals.dealer.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

/**
 * TODO: Add a class header comment!
 */
public class Utility {



    public static String formatPrice(Context context, String price)
    {
       /* double money = 100.1;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String moneyString = formatter.format(Double.parseDouble(price));*/

        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);

        return String.format(context.getString(R.string.price_symbol) + " " + nf.format(Double.parseDouble(price)).trim().replaceAll("\\.0*$", ""));

    }

    public static String formatYearOfManufacture(Context context, String year)
    {
        return String.format(context.getString(R.string.format_year) + " " + year);

    }

    public static String formatVehicleTitle(String year, String make, String model)
    {
        return String.format(year + " " + make+ " " + model);

    }

    public static String formatFuelTransmission(String fuel, String transmission)
    {
        return String.format(fuel + " / " + transmission);

    }

    public static String formatConditionDoorsCategory(Context context, String condition, String doors, String category)
    {
        return String.format(condition + ", " + doors  + " " +  context.getString(R.string.format_doors)+", " + category);

    }

    public static String formatExtraDetailsLabel(Context context)
    {
        return String.format(context.getString(R.string.format_extra_details_label));
    }

    public static String formatFeaturesLabel(Context context)
    {
        return String.format(context.getString(R.string.format_features_label));
    }

    public static String formatShareStr(Context context, String year, String make, String model, String price)
    {
        return String.format(context.getString(R.string.format_share_str)+
                " " + year +  ", " + make + " " + model +
                " " + context.getString(R.string.format_share_str_selling_at) + " " +
                price  + " " + context.getString(R.string.share_hash_tag));
    }


    public static String formatIntColor(Context context, String color)
    {
        return String.format(context.getString(R.string.format_int_color)  + " " +  color);
    }

    public static String formatExtColor(Context context, String color)
    {
        return String.format(context.getString(R.string.format_ext_color)  + " " +  color);
    }

    public static String formatMileage(Context context, String mileage)
    {
        return String.format(context.getString(R.string.format_mileage) + " " + mileage  + " " + context.getString(R.string.mileage_units));

    }

    public static String formatEngineSize(Context context, String engine_size)
    {
        return String.format(context.getString(R.string.format_engine_size)  + " " +  engine_size);
    }

    public static String formatVinNumber(Context context, String vinNumber)
    {
        return String.format(context.getString(R.string.format_vin_number)  + " " +  vinNumber);
    }

    public static String formatEmailSubject(Context context)
    {
        return String.format(context.getString(R.string.format_email_subject));
    }

    public static String formatEmailClient(Context context)
    {
        return String.format(context.getString(R.string.format_email_client));
    }

    public static String formatAnyMake(Context context)
    {
        return String.format(context.getString(R.string.format_any_make));
    }

    public static String formatAnyModel(Context context)
    {
        return String.format(context.getString(R.string.format_any_model));
    }

    public static String formatAnyYear(Context context)
    {
        return String.format(context.getString(R.string.format_any_year));
    }

    public static String formatNoVehicles(Context context)
    {
        return String.format(context.getString(R.string.format_no_vehicles));
    }

}
