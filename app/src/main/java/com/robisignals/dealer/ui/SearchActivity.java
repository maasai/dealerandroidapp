package com.robisignals.dealer.ui;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.robisignals.dealer.R;
import com.robisignals.dealer.provider.DealerContract;
import com.robisignals.dealer.provider.DealerDbHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;


public class SearchActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final String LOG_TAG = SearchActivity.class.getSimpleName();


    private static final int MAKES_LOADER = 1;
    private static final int MODELS_LOADER = 2;
    static final String SELECTED_MODEL_POSITION = "selectedModelPosition"; // Key for models spinner position on save/restore state event
    int mSavedModelPosition = AdapterView.INVALID_POSITION; // Class attribute to store saved models spinner position

    private static final int SEARCH_LOADER = 3;
    private Spinner mMakesSpinner;
    private Spinner mModelsSpinner;
    private Spinner mYearSpinner;
    private Button mSearchButton;
    private int mResultsCounter;

    private String mSelectedYear;
    private long mYearId;
    private String mModelId;
    private String mMakeId;

    public int getmResultsCounter() {
        return mResultsCounter;
    }

    public void setmResultsCounter(int mResultsCounter) {
        this.mResultsCounter = mResultsCounter;
    }

    private String[] mMakesProjection =  {
            BaseColumns._ID,
                DealerContract.MakesEntry.MAKE_ID,
                DealerContract.MakesEntry.MAKE_NAME
    };
    private String[] mModelsProjection  = {
            BaseColumns._ID,
            DealerContract.ModelsEntry.MAKE_ID,
            DealerContract.ModelsEntry.MODEL_ID,
            DealerContract.ModelsEntry.MODEL_NAME
    };

    SimpleCursorAdapter mMakesAdapter;
    SimpleCursorAdapter mModelsAdapter;
    ArrayAdapter<String> mYearAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) // If we're restoring...
        {
            // Retrieve models spinner saved position
            mSavedModelPosition = savedInstanceState.getInt(SELECTED_MODEL_POSITION, AdapterView.INVALID_POSITION);
        }
        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayUseLogoEnabled(true);


        //UI Views
        mMakesSpinner = (Spinner)findViewById(R.id.sp_search_make);
        mModelsSpinner = (Spinner) findViewById(R.id.sp_search_model);
        mYearSpinner = (Spinner)findViewById(R.id.sp_search_year);
        mSearchButton = (Button) findViewById(R.id.bt_search_search);

        //Makes Spinner
        //mMakesSpinner.setEnabled(false);
        String[] makeFrom = {DealerContract.MakesEntry.MAKE_NAME};
        int[] makeTo = {android.R.id.text1};
        mMakesAdapter = new SimpleCursorAdapter(this, R.layout.spinner_item, null, makeFrom, makeTo, 0);
        mMakesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        mMakesSpinner.setAdapter(mMakesAdapter);

        //Models Spinner
      //  mModelsSpinner.setEnabled(false);
        String[] modelFrom = {DealerContract.ModelsEntry.MODEL_NAME};
        int[] modelTo = {android.R.id.text1};
        mModelsAdapter = new SimpleCursorAdapter(this, R.layout.spinner_item, null, modelFrom, modelTo, 0);
        mModelsAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        mModelsSpinner.setAdapter(mModelsAdapter);

        //Years spinner
        ArrayList<String> years = new ArrayList<>();
        years.add(0, Utility.formatAnyYear(SearchActivity.this));
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = thisYear; i >= 1900; i--) {
            years.add(Integer.toString(i));
        }
        mYearAdapter = new ArrayAdapter<>(this, R.layout.spinner_item, years);
        mYearAdapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        mYearSpinner.setAdapter(mYearAdapter);




        mMakesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Log.d(LOG_TAG, "makes.onItemSelected(...) -> position = " + position);

                mModelsSpinner.setEnabled(false);
                Cursor make = (Cursor) parent.getItemAtPosition(position);
                String make_id = make.getString(make.getColumnIndexOrThrow(DealerContract.MakesEntry.MAKE_ID));

                Log.d(LOG_TAG, "make is is: " + make_id);
                // Update models spinners
                updateModels(make_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                Log.d(LOG_TAG, "makes::OnItemSelectedListener::onNothingSelected()");
            }
        });

        mSearchButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v)
            {
                mSelectedYear = mYearAdapter.getItem(mYearSpinner.getSelectedItemPosition());
                mYearId =  mYearAdapter.getItemId(mYearSpinner.getSelectedItemPosition());
                mMakeId = getSelectedField(mMakesAdapter, mMakesSpinner.getSelectedItemPosition(), DealerContract.MakesEntry.MAKE_ID, "");
                mModelId = getSelectedField(mModelsAdapter, mModelsSpinner.getSelectedItemPosition(), DealerContract.ModelsEntry.MODEL_ID, "");
                doSearch(mMakeId, mModelId, mYearId, mSelectedYear);
            }
        });

        getSupportLoaderManager().initLoader(MAKES_LOADER, null, this);

    }

    private void updateModels(String make_id)
    {
        Bundle bundle = new Bundle();
        bundle.putString(DealerContract.ModelsEntry.MAKE_ID, make_id);
        getSupportLoaderManager().restartLoader(MODELS_LOADER, bundle, this);
    }

    private void doSearch(String make_id, String model_id, long yearId, String year)
    {
        Bundle bundle = new Bundle();
        bundle.putLong("year_id", yearId);
        bundle.putString(DealerContract.ListingsEntry.MAKE_ID, make_id);
        bundle.putString(DealerContract.ListingsEntry.MODEL_ID, model_id);
        bundle.putString(DealerContract.ListingsEntry.REGISTRATION_YEAR, year);

        getSupportLoaderManager().restartLoader(SEARCH_LOADER, bundle, this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        if (mModelsAdapter != null && mModelsAdapter.getCount() > 0)
        {
            outState.putInt(SELECTED_MODEL_POSITION, mModelsSpinner.getSelectedItemPosition());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about){
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {

        switch (id){
            case MAKES_LOADER:{
                Log.d(LOG_TAG, "onCreateLoader<MAKES>");
                String sortOrder = DealerDbHelper.Tables.MAKES + "." + DealerContract.MakesEntry.MAKE_NAME + " ASC";

                return new CursorLoader(
                        this,
                        DealerContract.MakesEntry.CONTENT_URI,
                        mMakesProjection,
                        null,
                        null,
                        sortOrder
                );
            }
            case MODELS_LOADER:{
                Log.d(LOG_TAG, "onCreateLoader<MODELS>");
                String selection = DealerContract.ModelsEntry.MAKE_ID + "=?";
                String[] selectionArgs = new String[] { "-1" };
                if (args != null && args.containsKey(DealerContract.ModelsEntry.MAKE_ID))
                {
                    selectionArgs = new String[] { args.getString(DealerContract.ModelsEntry.MAKE_ID) };
                }

                String sortOrder = DealerDbHelper.Tables.MODELS + "." + DealerContract.ModelsEntry.MODEL_NAME + " ASC";


                return new CursorLoader(
                        this,
                        DealerContract.ModelsEntry.CONTENT_URI,
                        mModelsProjection,
                        selection,
                        selectionArgs,
                        sortOrder
                );
            }
            case SEARCH_LOADER:{
                Log.d(LOG_TAG, "onCreateLoader<SEARCH>");

                String selection = null;
                ArrayList<String> selectionArgsList = new ArrayList<String>();

                String makeId = "-1";
                String modelId = "-1";
                String year = null;
                int yearId = 0;

               if (args != null)
                {
                    if(args.containsKey(DealerContract.ListingsEntry.MAKE_ID)){
                        makeId = args.getString(DealerContract.ListingsEntry.MAKE_ID);
                    }
                    if(args.containsKey(DealerContract.ListingsEntry.MODEL_ID)){
                        modelId = args.getString(DealerContract.ListingsEntry.MODEL_ID);
                    }
                    if(args.containsKey(DealerContract.ListingsEntry.REGISTRATION_YEAR)){
                        year = args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR);
                    }
                    if(args.containsKey("year_id")){
                        yearId = (int)args.getLong("year_id");
                    }

                    //None is selected
                    //return all data
                    if(makeId.equalsIgnoreCase("-1") && modelId.equalsIgnoreCase("-1") && yearId == 0){
                        Log.d(LOG_TAG, "None is selected");
                        selectionArgsList.add("0");
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID + ">?" + " GROUP BY "+ DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                    }

                    //All are selected
                    else if(!makeId.equalsIgnoreCase("-1") && !modelId.equalsIgnoreCase("-1") && yearId != 0){
                        Log.d(LOG_TAG, "All are selected");
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MODEL_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));

                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MODEL_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.REGISTRATION_YEAR + "=?"
                                + " GROUP BY "+ DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                        }

                    //Only make is selected
                    else if (!makeId.equalsIgnoreCase("-1") && modelId.equalsIgnoreCase("-1") && yearId == 0){
                        Log.d(LOG_TAG, "Only make is selected");
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));

                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=?"
                                + " GROUP BY "+ DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                        Log.i(LOG_TAG, "the final args are: " + Arrays.toString(selectionArgsList.toArray(new String[selectionArgsList.size()])));

                    }

                    //Only both make and model selected
                    else if(!makeId.equalsIgnoreCase("-1") && !modelId.equalsIgnoreCase("-1") && yearId == 0){
                        Log.d(LOG_TAG, "Both make and model are selected");
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MODEL_ID + "=?"
                                + " GROUP BY "+ DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;

                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MODEL_ID));
                        Log.i(LOG_TAG, "the final args are: " + Arrays.toString(selectionArgsList.toArray(new String[selectionArgsList.size()])));
                    }

                    //only year is selected

                    else if (yearId != 0 && makeId.equalsIgnoreCase("-1") && modelId.equalsIgnoreCase("-1")){
                        Log.d(LOG_TAG, "Only year is selected");
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.REGISTRATION_YEAR + "=?"
                                + " GROUP BY "+ DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                    }

                    //Only year and make are selected
                    else if (!makeId.equalsIgnoreCase("-1") && yearId != 0 && modelId.equalsIgnoreCase("-1")){
                        Log.d(LOG_TAG, "Only year and make are selected");
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.REGISTRATION_YEAR + "=?"
                                + " GROUP BY "+ DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));

                    }

                }
                String sortOrder = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.CREATED_AT + " DESC";

                String[] projection = {
                        DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry._ID,
                        DealerContract.ListingsEntry.PRICE,
                        DealerContract.ListingsEntry.REGISTRATION_YEAR,
                        DealerContract.ListingsEntry.MILEAGE,
                        DealerContract.ListingsEntry.PRICE,
                        DealerDbHelper.Tables.LISTINGS + "." +  DealerContract.ListingsEntry.MAKE_ID,
                        DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MODEL_ID,
                        DealerContract.MakesEntry.MAKE_NAME,
                        DealerContract.ModelsEntry.MODEL_NAME,
                        DealerContract.VehicleImagesEntry.FILE_NAME
                };

                return new CursorLoader(
                        this,
                        DealerContract.ListingsSearchEntry.CONTENT_URI,
                        projection,
                        selection,
                        selectionArgsList.toArray(new String[selectionArgsList.size()]),
                        sortOrder
                );

            }
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch(loader.getId()){
            case MAKES_LOADER:
            {
                Log.d(LOG_TAG, "onLoadFinished<Makes>");

                MatrixCursor extras = new MatrixCursor(mMakesProjection);
                extras.addRow(new String[] { "-1", "-1", Utility.formatAnyMake(SearchActivity.this) });
                Cursor[] cursors = { extras, data };
                Cursor extendedMakesCursor = new MergeCursor(cursors);

                onLoadFinishedMakes(extendedMakesCursor);
                break;
            }
            case MODELS_LOADER:
            {
                Log.d(LOG_TAG, "onLoadFinished<Models>");

                MatrixCursor extras = new MatrixCursor(mModelsProjection);
                extras.addRow(new String[] { "-1", "-1", "-1", Utility.formatAnyModel(SearchActivity.this) });
                Cursor[] cursors = { extras, data };
                Cursor extendedModelsCursor = new MergeCursor(cursors);

                onLoadFinishedModels(extendedModelsCursor);
                break;
            }
            case SEARCH_LOADER:
            {
                int rowCount = data.getCount();
                mResultsCounter = data.getCount();
                setmResultsCounter(mResultsCounter);

                Log.d(LOG_TAG, "onLoadFinished<Search> ....rows are: " + mResultsCounter);


                if (getmResultsCounter() <= 0){
                    Toast.makeText(SearchActivity.this, Utility.formatNoVehicles(this), Toast.LENGTH_SHORT).show();

                }else{
                    Intent i = new Intent(SearchActivity.this, ResultsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(DealerContract.MakesEntry.MAKE_ID, mMakeId);
                    bundle.putString(DealerContract.ModelsEntry.MODEL_ID, mModelId);
                    bundle.putString(DealerContract.ListingsEntry.REGISTRATION_YEAR, mSelectedYear);
                    bundle.putLong("year_id", mYearId);
                    i.putExtras(bundle);
                    startActivity(i);
                }

                DatabaseUtils.dumpCursor(data);
                break;
            }

        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId())
        {
            case MAKES_LOADER:
                Log.d(LOG_TAG, "onLoaderReset<Makes>");
                mMakesAdapter.swapCursor(null);
                break;
            case MODELS_LOADER:
                Log.d(LOG_TAG, "onLoaderReset<Models>");
                mModelsAdapter.swapCursor(null);
                break;
        }
    }



    private void onLoadFinishedMakes(Cursor data)
    {
        mMakesAdapter.swapCursor(data);
        int rowCount = data.getCount();
        Log.d(LOG_TAG, "onLoadFinishedMakes(...) -> Makes data loaded. " + rowCount + " rows available");
        if (rowCount > 0)
        {
            mMakesSpinner.setEnabled(true);
        }
        else
        {
            // Both Spinners must be disabled
            mMakesSpinner.setEnabled(false);
            mModelsSpinner.setEnabled(false);
        }
    }

    private void onLoadFinishedModels(Cursor data)
    {
        mModelsAdapter.swapCursor(data);
        int rowCount = data.getCount();
        Log.d(LOG_TAG, "onLoadFinishedModels(...) -> Models data loaded. " + rowCount + " rows available");
        if (rowCount > 0)
        {
            if (mSavedModelPosition != AdapterView.INVALID_POSITION)
            {
                mModelsSpinner.setSelection(mSavedModelPosition);
                mSavedModelPosition = AdapterView.INVALID_POSITION;
            }
            else
            {
                mModelsSpinner.setSelection(0);
            }
            mModelsSpinner.setEnabled(true);
        }
        else
        {
            mModelsSpinner.setEnabled(false);
        }
    }

    private String getSelectedField(SimpleCursorAdapter adapter, int selectedPosition, String field, String defaultValue)
    {
        if(adapter.getCount() > 0)
        {
            Cursor cur = (Cursor)adapter.getItem(selectedPosition);
            return cur.getString(cur.getColumnIndex(field));
        }
        else
        {
            return defaultValue;
        }
    }
}
