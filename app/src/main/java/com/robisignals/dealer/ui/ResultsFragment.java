package com.robisignals.dealer.ui;


import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.robisignals.dealer.R;
import com.robisignals.dealer.provider.DealerContract;
import com.robisignals.dealer.provider.DealerDbHelper;
import com.robisignals.dealer.sync.SyncUtils;

import java.util.ArrayList;
import java.util.Arrays;

public class ResultsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String LOG_TAG = ResultsFragment.class.getSimpleName();

    private static final int SEARCH_LOADER = 4;
    private ListingAdapter mListingAdapter;

    private ListView mListView;
    private int mPosition = ListView.INVALID_POSITION;
    private static final String SELECTED_KEY = "selected_position";

    View rootView;
    private ProgressBar mProgressBar;
    private TextView mLoadingView;
    private TextView mLoadingMoreView;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callback {
        /**
         * DetailFragmentCallback for when an item has been selected.
         */
        public void onItemSelected(long listingId);
    }

    public ResultsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Add this line in order for this fragment to handle menu events.
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
       if (id == R.id.action_about){
            Intent i = new Intent(getActivity(), AboutActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mListingAdapter = new ListingAdapter(getActivity(), null, 0);

        rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mListView  = (ListView)rootView.findViewById(R.id.listView);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.pb_loading_main);
        mLoadingView = (TextView) rootView.findViewById(R.id.tv_main_loading);
        mLoadingMoreView = (TextView) rootView.findViewById(R.id.tv_main_loading_more);

        mListView.setAdapter(mListingAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Cursor cursor = mListingAdapter.getCursor();

                if (cursor != null && cursor.moveToPosition(position)) {
                    ((Callback)getActivity())
                            .onItemSelected(cursor.getLong(cursor.getColumnIndex(DealerContract.ListingsEntry._ID)));
                }
                mPosition = position;

            }
        });

        // If there's instance state, mine it for useful information.
        // The end-goal here is that the user never knows that turning their device sideways
        // does crazy lifecycle related things.  It should feel like some stuff stretched out,
        // or magically appeared to take advantage of room, but data or place in the app was never
        // actually *lost*.
        if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY)) {
            // The listview probably hasn't even been populated yet.  Actually perform the
            // swapout in onLoadFinished.
            mPosition = savedInstanceState.getInt(SELECTED_KEY);
        }

        if (mPosition != ListView.INVALID_POSITION) {
            // If we don't need to restart the loader, and there's a desired position to restore
            // to, do so now.
            mListView.smoothScrollToPosition(mPosition);
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if(getArguments() != null){
            Bundle bundle = new Bundle();
            bundle.putLong("year_id", getArguments().getLong("year_id"));
            bundle.putString(DealerContract.ListingsEntry.MAKE_ID, getArguments().getString(DealerContract.MakesEntry.MAKE_ID));
            bundle.putString(DealerContract.ListingsEntry.MODEL_ID, getArguments().getString(DealerContract.ModelsEntry.MODEL_ID));
            bundle.putString(DealerContract.ListingsEntry.REGISTRATION_YEAR, getArguments().getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));

            getLoaderManager().initLoader(SEARCH_LOADER, bundle, this);

        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
       /* if (mPosition != ListView.INVALID_POSITION) {
            // If we don't need to restart the loader, and there's a desired position to restore
            // to, do so now.
            mListView.smoothScrollToPosition(mPosition);
        }*/
        super.onResume();
       // getLoaderManager().restartLoader(SEARCH_LOADER, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // When tablets rotate, the currently selected list item needs to be saved.
        // When no item is selected, mPosition will be set to Listview.INVALID_POSITION,
        // so check for that before storing.
        if (mPosition != ListView.INVALID_POSITION) {
            outState.putInt(SELECTED_KEY, mPosition);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case SEARCH_LOADER: {
                Log.d(LOG_TAG, "onCreateLoader<SEARCH>");

                String selection = null;
                ArrayList<String> selectionArgsList = new ArrayList<String>();

                String makeId = "-1";
                String modelId = "-1";
                String year = null;
                int yearId = 0;

                if (args != null) {
                    if (args.containsKey(DealerContract.ListingsEntry.MAKE_ID)) {
                        makeId = args.getString(DealerContract.ListingsEntry.MAKE_ID);
                    }
                    if (args.containsKey(DealerContract.ListingsEntry.MODEL_ID)) {
                        modelId = args.getString(DealerContract.ListingsEntry.MODEL_ID);
                    }
                    if (args.containsKey(DealerContract.ListingsEntry.REGISTRATION_YEAR)) {
                        year = args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR);
                    }
                    if (args.containsKey("year_id")) {
                        yearId = (int) args.getLong("year_id");
                    }

                    //None is selected
                    //return all data
                    if (makeId.equalsIgnoreCase("-1") && modelId.equalsIgnoreCase("-1") && yearId == 0) {
                        Log.d(LOG_TAG, "None is selected");
                        selectionArgsList.add("0");
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID + ">?" + " GROUP BY " + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                    }

                    //All are selected
                    else if (!makeId.equalsIgnoreCase("-1") && !modelId.equalsIgnoreCase("-1") && yearId != 0) {
                        Log.d(LOG_TAG, "All are selected");
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MODEL_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));

                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MODEL_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.REGISTRATION_YEAR + "=?"
                                + " GROUP BY " + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                    }

                    //Only make is selected
                    else if (!makeId.equalsIgnoreCase("-1") && modelId.equalsIgnoreCase("-1") && yearId == 0) {
                        Log.d(LOG_TAG, "Only make is selected");
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));

                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=?"
                                + " GROUP BY " + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                        Log.i(LOG_TAG, "the final args are: " + Arrays.toString(selectionArgsList.toArray(new String[selectionArgsList.size()])));

                    }

                    //Only both make and model selected
                    else if (!makeId.equalsIgnoreCase("-1") && !modelId.equalsIgnoreCase("-1") && yearId == 0) {
                        Log.d(LOG_TAG, "Both make and model are selected");
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MODEL_ID + "=?"
                                + " GROUP BY " + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;

                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MODEL_ID));
                        Log.i(LOG_TAG, "the final args are: " + Arrays.toString(selectionArgsList.toArray(new String[selectionArgsList.size()])));
                    }

                    //only year is selected
                    else if (yearId != 0 && makeId.equalsIgnoreCase("-1") && modelId.equalsIgnoreCase("-1")) {
                        Log.d(LOG_TAG, "Only year is selected");
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.REGISTRATION_YEAR + "=?"
                                + " GROUP BY " + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                    }

                    //Only year and make are selected
                    else if (!makeId.equalsIgnoreCase("-1") && yearId != 0 && modelId.equalsIgnoreCase("-1")) {
                        Log.d(LOG_TAG, "Only year and make are selected");
                        selection = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID + "=? AND "
                                + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.REGISTRATION_YEAR + "=?"
                                + " GROUP BY " + DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.LISTING_ID;
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.MAKE_ID));
                        selectionArgsList.add(args.getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));

                    }

                }
                String sortOrder = DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.CREATED_AT + " DESC";

                String[] projection = {
                        DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry._ID,
                        DealerContract.ListingsEntry.PRICE,
                        DealerContract.ListingsEntry.REGISTRATION_YEAR,
                        DealerContract.ListingsEntry.MILEAGE,
                        DealerContract.ListingsEntry.PRICE,
                        DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MAKE_ID,
                        DealerDbHelper.Tables.LISTINGS + "." + DealerContract.ListingsEntry.MODEL_ID,
                        DealerContract.MakesEntry.MAKE_NAME,
                        DealerContract.ModelsEntry.MODEL_NAME,
                        DealerContract.VehicleImagesEntry.FILE_NAME
                };

                return new CursorLoader(
                        getActivity(),
                        DealerContract.ListingsSearchEntry.CONTENT_URI,
                        projection,
                        selection,
                        selectionArgsList.toArray(new String[selectionArgsList.size()]),
                        sortOrder
                );
            }
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int rowCount = data.getCount();
        Log.d(LOG_TAG, "onLoadFinished<Search> ....rows are: " + rowCount);
        DatabaseUtils.dumpCursor(data);


        if (data.getCount() == 0) {
            mProgressBar.setVisibility(View.VISIBLE);
            mLoadingView.setVisibility(View.VISIBLE);
            mLoadingMoreView.setVisibility(View.VISIBLE);

            SyncUtils.triggerRefresh();
        }else {
            mProgressBar.setVisibility(View.GONE);
            mLoadingView.setVisibility(View.GONE);
            mLoadingMoreView.setVisibility(View.GONE);
        }
        mListingAdapter.swapCursor(data);

        if (mPosition != ListView.INVALID_POSITION) {
            // If we don't need to restart the loader, and there's
            // a desired position to restore to, do so now.
            mListView.smoothScrollToPosition(mPosition);
          /*  if (MainActivity.mTwoPane) {
                mListView.post(new Runnable() {
                    @Override
                    public void run() {
                        mListView.performItemClick(rootView, mPosition, mListView.getAdapter().getItemId(mPosition));
                    }
                });
            }*/
        } else if (MainActivity.mTwoPane) {
            mListView.post(new Runnable() {
                @Override
                public void run() {
                    mListView.performItemClick(rootView, 0, mListView.getAdapter().getItemId(0));
                }
            });
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mListingAdapter.swapCursor(null);

    }
}
