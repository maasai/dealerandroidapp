package com.robisignals.dealer.ui;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.robisignals.dealer.R;
import com.robisignals.dealer.provider.DealerContract;

public class AboutActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = AboutActivity.class.getSimpleName();
    private static final int ABOUT_LOADER = 0;

    private TextView mCompanyNameView;
    private TextView mCompanyDescriptionView;
    private TextView mCompanyAddressView;
    private TextView mCompanyPhoneView;
    private TextView mCompanyEmailView;
    private TextView mCompanyWebsiteView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        getSupportLoaderManager().initLoader(ABOUT_LOADER, null, this);

        mCompanyNameView = (TextView) findViewById(R.id.tv_about_company_name);
        mCompanyDescriptionView = (TextView) findViewById(R.id.tv_about_company_description);
        mCompanyAddressView = (TextView) findViewById(R.id.tv_about_address);
        mCompanyPhoneView = (TextView) findViewById(R.id.tv_about_phone);
        mCompanyEmailView = (TextView) findViewById(R.id.tv_about_email);
        mCompanyWebsiteView = (TextView) findViewById(R.id.tv_about_website);


        mCompanyEmailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = Utility.formatEmailSubject(AboutActivity.this);
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", mCompanyEmailView.getText().toString(), null));
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                startActivity(Intent.createChooser(intent, Utility.formatEmailClient(AboutActivity.this)));
            }
        });

        mCompanyPhoneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                String p = "tel:" + mCompanyPhoneView.getText().toString();
                i.setData(Uri.parse(p));
                startActivity(i);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportLoaderManager().restartLoader(ABOUT_LOADER, null, this);
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {

        String limit = "LIMIT 1";

        String[] projection  = {
                DealerContract.SettingsEntry.COMPANY_NAME,
                DealerContract.SettingsEntry.COMPANY_DESCRIPTION,
                DealerContract.SettingsEntry.COMPANY_ADDRESS,
                DealerContract.SettingsEntry.COMPANY_PHONE,
                DealerContract.SettingsEntry.COMPANY_EMAIL,
                DealerContract.SettingsEntry.COMPANY_WEBSITE,
        };

        return new CursorLoader(
                this,
                DealerContract.SettingsEntry.CONTENT_URI,
                projection,
                null,
                null,
                null
               );

    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor) {

        //read and update view here
        Log.d(LOG_TAG, "Just fetched a listing" + DatabaseUtils.dumpCursorToString(cursor));
        getLoaderManager().destroyLoader(ABOUT_LOADER);

        if (cursor != null && cursor.moveToFirst()) {
            String companyName = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_NAME));
            String companyDescription = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_DESCRIPTION));
            String companyAddress = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_ADDRESS));
            String companyPhone = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_PHONE));
            String companyEmail = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_EMAIL));
            String companyWebsite = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_WEBSITE));


            if (companyName != null){
                mCompanyNameView.setText(companyName);
            }else {
                mCompanyNameView.setVisibility(View.GONE);
            }

            if (companyDescription != null){
                mCompanyDescriptionView.setText(Html.fromHtml(companyDescription));
            }else {
                mCompanyDescriptionView.setVisibility(View.GONE);
            }

            if (companyAddress != null){
                mCompanyAddressView.setText(companyAddress);
            }else {
                mCompanyAddressView.setVisibility(View.GONE);
            }

            if (companyPhone != null  && !companyPhone.isEmpty()){
                mCompanyPhoneView.setVisibility(View.VISIBLE);
                mCompanyPhoneView.setText(companyPhone);
            }else {
                mCompanyPhoneView.setVisibility(View.GONE);
            }

            if (companyEmail != null){
                mCompanyEmailView.setVisibility(View.VISIBLE);
                mCompanyEmailView.setText(companyEmail);
            }else {
                mCompanyEmailView.setVisibility(View.GONE);
            }


            if (companyWebsite != null  && !companyWebsite.isEmpty()){
                mCompanyWebsiteView.setVisibility(View.VISIBLE);
                mCompanyWebsiteView.setText(companyWebsite);
            }else {
                mCompanyWebsiteView.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
