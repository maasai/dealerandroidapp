package com.robisignals.dealer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.robisignals.dealer.R;
import com.robisignals.dealer.sync.SyncUtils;

public class MainActivity extends ActionBarActivity implements ListingFragment.Callback {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();
    private String mActivityTitle;
    public static boolean mTwoPane = false;

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

         if(findViewById(R.id.listing_detail_container) != null){
            //if the detail continer is present, we are in two pane mode
            mTwoPane = true;

            if (savedInstanceState == null ){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.listing_detail_container, new DetailFragment())
                        .commit();
            }

        }else{
            mTwoPane = false;
        }


       SyncUtils.CreateSyncAccount(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(long listingId) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle args = new Bundle();
            args.putLong(DetailActivity.LISTING_ID_KEY, listingId);

            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(args);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.listing_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, DetailActivity.class)
                    .putExtra(DetailActivity.LISTING_ID_KEY, listingId);
            startActivity(intent);
        }
    }
}
