package com.robisignals.dealer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.robisignals.dealer.R;
import com.robisignals.dealer.provider.DealerContract;

public class ResultsActivity extends ActionBarActivity implements ResultsFragment.Callback {

    public static final String LOG_TAG = ResultsActivity.class.getSimpleName();
    private String mActivityTitle;
    public static boolean mTwoPane = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        if (savedInstanceState == null) {
            Bundle bundle = new Bundle();
            bundle.putString(DealerContract.MakesEntry.MAKE_ID, getIntent().getExtras().getString(DealerContract.MakesEntry.MAKE_ID));
            bundle.putString(DealerContract.ModelsEntry.MODEL_ID, getIntent().getExtras().getString(DealerContract.ModelsEntry.MODEL_ID));
            bundle.putString(DealerContract.ListingsEntry.REGISTRATION_YEAR, getIntent().getExtras().getString(DealerContract.ListingsEntry.REGISTRATION_YEAR));
            bundle.putLong("year_id", getIntent().getExtras().getLong("year_id"));

             ResultsFragment fragment = new ResultsFragment();
            fragment.setArguments(bundle);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_results, fragment)
                    .commit();
        }

        if(findViewById(R.id.listing_detail_container) != null){
            //if the detail continer is present, we are in two pane mode
            mTwoPane = true;

            if (savedInstanceState == null ){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.listing_detail_container, new DetailFragment())
                        .commit();
            }

        }else{
            mTwoPane = false;
        }

   }

    @Override
    public void onItemSelected(long listingId) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle args = new Bundle();
            args.putLong(DetailActivity.LISTING_ID_KEY, listingId);

            DetailFragment fragment = new DetailFragment();
            fragment.setArguments(args);

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.listing_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, DetailActivity.class)
                    .putExtra(DetailActivity.LISTING_ID_KEY, listingId);
            startActivity(intent);
        }
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
