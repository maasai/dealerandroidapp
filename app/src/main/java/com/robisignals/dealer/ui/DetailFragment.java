package com.robisignals.dealer.ui;

import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.robisignals.dealer.R;
import com.robisignals.dealer.provider.DealerContract;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * TODO: Add a class header comment!
 */
public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    private static final int DETAIL_LOADER = 0;
    private static final String IMAGE_DATA_EXTRA = "resId";
    //private String mListingId;
    private Long mListingId = 0l;
    private static final String LISTING_KEY = "listing_key";
    private ShareActionProvider mShareActionProvider;
    private static final String SHARE_HASHTAG = " #CarForSale";
    private String mShareStr;
    private ImageView mImageView;
    private TextView mTitleView;
    private TextView mFuelTransmission;
    private TextView mPriceView;
    private TextView mExtraDetailsView;
    private TextView mFeaturesView;
    private TextView mColorConditionCategoryView;
    private TextView mDetailsLabel;
    private TextView mFeaturesLabel;
    private TextView mIntColorView;
    private TextView mExtColorView;
    private TextView mMileageView;
    private TextView mEngineSizeView;
    private TextView mVinNumberView;
    private Button mCompanyPhone;
    private Button mCompanyEmail;

    private String companyPhone;
    private String companyEmail;

    private String year;
    private String make;
    private String model;

    private String[] urls  = null;
    private ArrayList<String> images;



    public DetailFragment() {
       setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_detail, menu);

        // Retrieve the share menu item
        MenuItem menuItem = menu.findItem(R.id.action_share);

        // Get the provider and hold onto it to set/change the share intent.
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

        // If onLoadFinished happens before this, we can go ahead and set the share intent now.
        if (mShareStr != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
    }

    private Intent createShareForecastIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mShareStr);
        return shareIntent;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(LISTING_KEY, mListingId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle arguments = getArguments();

        if (arguments != null) {
            mListingId = arguments.getLong(DetailActivity.LISTING_ID_KEY);
        }

        if (savedInstanceState != null) {
            mListingId = savedInstanceState.getLong(LISTING_KEY);
        }

        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        mImageView = (ImageView) rootView.findViewById(R.id.tv_detail_image);
        mPriceView = (TextView) rootView.findViewById(R.id.tv_detail_price);
        mExtraDetailsView = (TextView) rootView.findViewById(R.id.tv_detail_extra_details);
        mFeaturesView = (TextView) rootView.findViewById(R.id.tv_detail_features);
        mTitleView = (TextView) rootView.findViewById(R.id.tv_detail_title);
        mFuelTransmission = (TextView) rootView.findViewById(R.id.tv_detail_fuel_transmission);
        mColorConditionCategoryView = (TextView) rootView.findViewById(R.id.tv_detail_color_condition_category);
        mDetailsLabel = (TextView) rootView.findViewById(R.id.tv_details_label_extra_details);
        mFeaturesLabel = (TextView) rootView.findViewById(R.id.tv_details_label_features);
        mIntColorView = (TextView) rootView.findViewById(R.id.tv_details_int_color);
        mExtColorView = (TextView) rootView.findViewById(R.id.tv_details_ext_color);
        mMileageView = (TextView) rootView.findViewById(R.id.tv_details_mileage);
        mEngineSizeView = (TextView) rootView.findViewById(R.id.tv_details_engine_size);
        mVinNumberView = (TextView) rootView.findViewById(R.id.tv_details_vin_number);
        mCompanyPhone = (Button) rootView.findViewById(R.id.bt_details_call);
        mCompanyEmail = (Button) rootView.findViewById(R.id.bt_details_email);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getActivity(), "clicked on image" + Arrays.toString(urls), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(getActivity(), ImagesActivity.class);
                //i.putStringArrayListExtra("images", images);
                i.putExtra("images", urls);
                startActivity(i);
            }
        });

        mCompanyPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_DIAL);
                String p = "tel:" + companyPhone;
                i.setData(Uri.parse(p));
                startActivity(i);
            }
        });


        mCompanyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = year + ", " + make + " " + model;
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", companyEmail, null));
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                startActivity(Intent.createChooser(intent, Utility.formatEmailClient(getActivity())));

            }
        });

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(DetailActivity.LISTING_ID_KEY) &&
                mListingId != null) {
           getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mListingId = savedInstanceState.getLong(LISTING_KEY);
        }

        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(DetailActivity.LISTING_ID_KEY)) {
            getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        }
    }



    @Override
    public Loader onCreateLoader(int id, Bundle args) {

        Uri listingUri = DealerContract.ListingsEntry.buildListingUri(mListingId);

        String[] arguments = { String.valueOf(mListingId)  };

        String[] projection = {
                DealerContract.ListingsEntry.PRICE,
                DealerContract.ListingsEntry.REGISTRATION_YEAR,
                DealerContract.ListingsEntry.ENGINE_SIZE,
                DealerContract.ListingsEntry.VIN_NUMBER,
                DealerContract.ListingsEntry.DOORS,
                DealerContract.ListingsEntry.FEATURED,
                DealerContract.ListingsEntry.EXTRA_DETAILS,
                DealerContract.ListingsEntry.ACTIVE,
                DealerContract.ListingsEntry.MILEAGE,
                "GROUP_CONCAT(DISTINCT "+DealerContract.FeaturesEntry.FEATURE_NAME+") AS feature_name",
                "GROUP_CONCAT(DISTINCT "+DealerContract.VehicleImagesEntry.FILE_NAME+") AS file_name",
                "c1.color_name AS ext_color_name",
                "c2.color_name AS int_color_name",
                DealerContract.MakesEntry.MAKE_NAME,
                DealerContract.ModelsEntry.MODEL_NAME,
                DealerContract.TransmissionsEntry.TRANSMISSION_NAME,
                DealerContract.CategoriesEntry.CATEGORY_NAME,
                DealerContract.ConditionsEntry.CONDITION_NAME,
                DealerContract.FuelsEntry.FUEL_NAME,
                DealerContract.SettingsEntry.COMPANY_PHONE,
                DealerContract.SettingsEntry.COMPANY_EMAIL,
        };

        // Now create and return a CursorLoader that will take care of
        // creating a Cursor for the data being displayed.
        return new CursorLoader(
                getActivity(),
                listingUri,
                projection,
                null,
                arguments,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor) {

        String imgBasePath;

        if (cursor != null && cursor.moveToFirst()) {
            String fileNames = cursor.getString(cursor.getColumnIndex("file_name"));

            if(fileNames != null && !fileNames.isEmpty()){
                urls = fileNames.split(",", -1);

            }

            if (urls != null){
                imgBasePath = "http://dealer.twigahost.com/assets/img/vehicle/"+urls[0];
                Picasso.with(getActivity()).load(imgBasePath).into(mImageView);
            }

            year = cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.REGISTRATION_YEAR));
            make = cursor.getString(cursor.getColumnIndex(DealerContract.MakesEntry.MAKE_NAME));
            model = cursor.getString(cursor.getColumnIndex(DealerContract.ModelsEntry.MODEL_NAME));
            String fuel = cursor.getString(cursor.getColumnIndex(DealerContract.FuelsEntry.FUEL_NAME));
            String transmission = cursor.getString(cursor.getColumnIndex(DealerContract.TransmissionsEntry.TRANSMISSION_NAME));
            String price = cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.PRICE));
            String int_color = cursor.getString(cursor.getColumnIndex("int_color_name"));
            String ext_color = cursor.getString(cursor.getColumnIndex("ext_color_name"));
            String engine_size = cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.ENGINE_SIZE));
            String vin_number = cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.VIN_NUMBER));
            String mileage =  cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.MILEAGE));
            String doors = cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.DOORS));
            String condition = cursor.getString(cursor.getColumnIndex(DealerContract.ConditionsEntry.CONDITION_NAME));
            String category = cursor.getString(cursor.getColumnIndex(DealerContract.CategoriesEntry.CATEGORY_NAME));

            String details = cursor.getString(cursor.getColumnIndex(DealerContract.ListingsEntry.EXTRA_DETAILS));
            String features = cursor.getString(cursor.getColumnIndex(DealerContract.FeaturesEntry.FEATURE_NAME));

            companyPhone = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_PHONE));
            companyEmail = cursor.getString(cursor.getColumnIndex(DealerContract.SettingsEntry.COMPANY_EMAIL));

            if (year != null && make != null && model != null){
                mTitleView.setText(Utility.formatVehicleTitle(year, make, model));
            }

            if (fuel != null && transmission != null){
                mFuelTransmission.setText(Utility.formatFuelTransmission(fuel, transmission));
            }

            if (price != null && !price.isEmpty()){
                mPriceView.setText(Utility.formatPrice(getActivity(), price));
            }

            if (condition != null && doors != null  && category != null){
                mColorConditionCategoryView.setText(Utility.formatConditionDoorsCategory(getActivity(), condition, doors, category));
            }

            if (int_color != null && !int_color.isEmpty()){
                mIntColorView.setVisibility(View.VISIBLE);
                mIntColorView.setText(Utility.formatIntColor(getActivity(), int_color));
            }else {
                mIntColorView.setVisibility(View.GONE);
            }

            if (ext_color != null && !ext_color.isEmpty()){
                mExtColorView.setVisibility(View.VISIBLE);
                mExtColorView.setText(Utility.formatExtColor(getActivity(), ext_color));
            }else {
                mExtColorView.setVisibility(View.GONE);
            }

            if (mileage != null && !mileage.isEmpty()){
                mMileageView.setVisibility(View.VISIBLE);
                mMileageView.setText(Utility.formatMileage(getActivity(), mileage));
            }else {
                mMileageView.setVisibility(View.GONE);
            }


            if (engine_size != null && !engine_size.isEmpty()){
                mEngineSizeView.setVisibility(View.VISIBLE);
                mEngineSizeView.setText(Utility.formatEngineSize(getActivity(), engine_size));
            }else {
                mEngineSizeView.setVisibility(View.GONE);
            }


            if (vin_number != null && !vin_number.isEmpty()){
                mVinNumberView.setVisibility(View.VISIBLE);
                mVinNumberView.setText(Utility.formatVinNumber(getActivity(), vin_number));
            }else {
                mVinNumberView.setVisibility(View.GONE);
            }


            if (details !=null && !details.isEmpty()){
                mExtraDetailsView.setVisibility(View.VISIBLE);
                mDetailsLabel.setVisibility(View.VISIBLE);
                mExtraDetailsView.setText(Html.fromHtml(details));
                mDetailsLabel.setText(Utility.formatExtraDetailsLabel(getActivity()));
            }else {
                mDetailsLabel.setVisibility(View.GONE);
                mExtraDetailsView.setVisibility(View.GONE);
            }

            if (features !=null && !features.isEmpty()){
                mFeaturesLabel.setVisibility(View.VISIBLE);
                mFeaturesView.setVisibility(View.VISIBLE);
                mFeaturesView.setText(features);
                mFeaturesLabel.setText(Utility.formatFeaturesLabel(getActivity()));

            }else {
                mFeaturesLabel.setVisibility(View.GONE);
                mFeaturesView.setVisibility(View.GONE);
            }

            if (companyPhone!=null && companyPhone.trim().length() > 0){
                mCompanyPhone.setVisibility(View.VISIBLE);
            }else {
                mCompanyPhone.setVisibility(View.GONE);
            }

            if (companyEmail != null && !companyEmail.isEmpty()){
                mCompanyEmail.setVisibility(View.VISIBLE);
            }else {
                mCompanyEmail.setVisibility(View.GONE);
            }

            // We still need this for the share intent
         //   mShareStr = "Hey, check this out..." + year +  ", " + make + " " + model  +" Selling at: "+ price;

            mShareStr = Utility.formatShareStr(getActivity(), year, make, model, price);

        }
        //read and update view here
        Log.d(LOG_TAG, "Just fetched a listing" + DatabaseUtils.dumpCursorToString(cursor));
        getLoaderManager().destroyLoader(DETAIL_LOADER);

        // If onCreateOptionsMenu has already happened, we need to update the share intent now.
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }
}
