package com.robisignals.dealer.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.robisignals.dealer.R;
import com.robisignals.dealer.provider.DealerContract;
import com.squareup.picasso.Picasso;

/**
 * TODO: Add a class header comment!
 */
public class ListingAdapter extends CursorAdapter{

    /**
     * Cache of the children views for a forecast list item.
     */
    public static class ViewHolder {

        public final ImageView imageView;
        public final TextView yearView;
        public final TextView makeView;
        public final TextView modelView;
        public final TextView mileageView;
        public final TextView priceView;

        public ViewHolder(View view) {
            imageView = (ImageView) view.findViewById(R.id.list_item_icon);
            yearView = (TextView) view.findViewById(R.id.tv_year);
            makeView = (TextView) view.findViewById(R.id.tv_make);
            modelView = (TextView) view.findViewById(R.id.tv_model);
            mileageView = (TextView) view.findViewById(R.id.tv_mileage);
            priceView = (TextView) view.findViewById(R.id.tv_price);

        }
    }


    public ListingAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Choose the layout type
        int viewType = getItemViewType(cursor.getPosition());


        int layoutId = R.layout.list_item_card;

        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        int priceIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.PRICE);
        String price = cursor.getString(priceIndex);

        int mileageIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.MILEAGE);
        String mileage = cursor.getString(mileageIndex);

        int yearIndex = cursor.getColumnIndex(DealerContract.ListingsEntry.REGISTRATION_YEAR);
        String year = cursor.getString(yearIndex);

       int makeIdIndex = cursor.getColumnIndex(DealerContract.MakesEntry.MAKE_NAME);
       String makeName = cursor.getString(makeIdIndex);

        int modelIdIndex = cursor.getColumnIndex(DealerContract.ModelsEntry.MODEL_NAME);
        String modelName = cursor.getString(modelIdIndex);

       int imageIndex = cursor.getColumnIndex(DealerContract.VehicleImagesEntry.FILE_NAME);
       String fileName = cursor.getString(imageIndex);

        String imgBasePath = "http://dealer.twigahost.com/assets/img/vehicle/thumb/"+fileName;



        Picasso.with(context).load(imgBasePath).placeholder(R.drawable.soon_l).into(viewHolder.imageView);


        viewHolder.makeView.setText(makeName);
        viewHolder.modelView.setText(modelName);
        viewHolder.mileageView.setText(Utility.formatMileage(context, mileage));
        viewHolder.priceView.setText(Utility.formatPrice(context, price));
        viewHolder.yearView.setText(Utility.formatYearOfManufacture(context, year));


       }
}
