package com.robisignals.dealer.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.robisignals.dealer.provider.DealerContract.*;

/**
 * TODO: Add a class header comment!
 */
public class DealerProvider extends ContentProvider {

    private static final String LOG_TAG = DealerProvider.class.getSimpleName();
    DealerDbHelper mOpenHelper;

     private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int COLOR = 100;
    private static final int COLOR_ID = 101;

    private static final int CATEGORY = 200;
    private static final int CATEGORY_ID = 201;

    private static final int CONDITION = 300;
    private static final int CONDITION_ID = 301;

    private static final int FEATURE = 400;
    private static final int FEATURE_ID = 401;

    private static final int FUEL = 500;
    private static final int FUEL_ID = 501;

    private static final int LISTING = 600;
    private static final int LISTING_ID = 601;

    private static final int MAKE = 700;
    private static final int MAKE_ID = 701;

    private static final int MODEL = 800;
    private static final int MODEL_ID = 801;

    private static final int TRANSMISSION = 900;
    private static final int TRANSMISSION_ID = 901;

    private static final int SETTING = 1000;
    private static final int SETTING_ID = 1001;

    private static final int FEATURELISTING = 1100;
    private static final int FEATURELISTING_ID = 1101;

    private static final int VEHICLE_IMAGE = 1200;
    private static final int VEHICLE_IMAGE_ID = 1201;

    private static final int LISTINGS_SEARCH = 1202;

    /**
     * Build and return a {@link UriMatcher} that catches all {@link Uri}
     * variations supported by this {@link ContentProvider}.
     */
    private static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = DealerContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, DealerContract.PATH_COLOR, COLOR);
        matcher.addURI(authority, DealerContract.PATH_COLOR + "/*", COLOR_ID);

        matcher.addURI(authority, DealerContract.PATH_CATEGORY, CATEGORY);
        matcher.addURI(authority, DealerContract.PATH_CATEGORY + "/*", CATEGORY_ID);

        matcher.addURI(authority, DealerContract.PATH_CONDITION, CONDITION);
        matcher.addURI(authority, DealerContract.PATH_CONDITION + "/*", CONDITION_ID);

        matcher.addURI(authority, DealerContract.PATH_FEATURE, FEATURE);
        matcher.addURI(authority, DealerContract.PATH_FEATURE + "/*", FEATURE_ID);

        matcher.addURI(authority, DealerContract.PATH_FUEL, FUEL);
        matcher.addURI(authority, DealerContract.PATH_FUEL + "/*", FUEL_ID);

        matcher.addURI(authority, DealerContract.PATH_LISTING, LISTING);
        matcher.addURI(authority, DealerContract.PATH_LISTING + "/*", LISTING_ID);

        matcher.addURI(authority, DealerContract.PATH_MAKE, MAKE);
        matcher.addURI(authority, DealerContract.PATH_MAKE + "/*", MAKE_ID);

        matcher.addURI(authority, DealerContract.PATH_MODEL, MODEL);
        matcher.addURI(authority, DealerContract.PATH_MODEL + "/*", MODEL_ID);

        matcher.addURI(authority, DealerContract.PATH_TRANSMISSION, TRANSMISSION);
        matcher.addURI(authority, DealerContract.PATH_TRANSMISSION + "/*", TRANSMISSION_ID);

        matcher.addURI(authority, DealerContract.PATH_SETTINGS, SETTING);
        matcher.addURI(authority, DealerContract.PATH_SETTINGS + "/*", SETTING_ID);

        matcher.addURI(authority, DealerContract.PATH_FEATURE_LISTINGS, FEATURELISTING);
        matcher.addURI(authority, DealerContract.PATH_FEATURE_LISTINGS + "/*", FEATURELISTING_ID);

        matcher.addURI(authority, DealerContract.PATH_VEHICLE_IMAGES, VEHICLE_IMAGE);
        matcher.addURI(authority, DealerContract.PATH_VEHICLE_IMAGES + "/*", VEHICLE_IMAGE_ID);

        matcher.addURI(authority, DealerContract.PATH_LISTINGS_SEARCH, LISTINGS_SEARCH);

        return matcher;
    }




    @Override
    public boolean onCreate() {
        mOpenHelper = new DealerDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //given a uri, this will know how to act accordingly
        Cursor retCursor;

        switch (sUriMatcher.match(uri)){
            case COLOR:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.COLORS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CONDITION:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.CONDITIONS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CATEGORY:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.CATEGORIES,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case FEATURE:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.FEATURES,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case FUEL:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.FUELS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case LISTING:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.LISTINGS_JOIN_MAKES,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case LISTINGS_SEARCH:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.LISTINGS_SEARCH_QUERY,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case LISTING_ID:{
                Log.d(LOG_TAG, "Fetching at: "+ uri);
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.LISTINGS_ID_JOIN_ALL,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case MAKE:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.MAKES,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case MODEL:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.MODELS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case TRANSMISSION:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.TRANSMISSIONS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SETTING:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        true,
                        DealerDbHelper.Tables.SETTINGS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder,
                        "1"
                );
                break;
            }
            case FEATURELISTING:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.FEATURE_LISTINGS,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            case VEHICLE_IMAGE:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        DealerDbHelper.Tables.VEHICLE_IMAGES,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;

    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);

        switch (match){
            case COLOR:
                return ColorsEntry.CONTENT_TYPE;
            case COLOR_ID:
                return ColorsEntry.CONTENT_ITEM_TYPE;
            case CATEGORY:
                return CategoriesEntry.CONTENT_TYPE;
            case CATEGORY_ID:
                return CategoriesEntry.CONTENT_ITEM_TYPE;
            case CONDITION:
                return ConditionsEntry.CONTENT_TYPE;
            case CONDITION_ID:
                return ConditionsEntry.CONTENT_ITEM_TYPE;
            case FEATURE:
                return FeaturesEntry.CONTENT_TYPE;
            case FEATURE_ID:
                return FeaturesEntry.CONTENT_ITEM_TYPE;
            case FUEL:
                return FuelsEntry.CONTENT_TYPE;
            case FUEL_ID:
                return FuelsEntry.CONTENT_ITEM_TYPE;
            case LISTING:
                return ListingsEntry.CONTENT_TYPE;
            case LISTING_ID:
                return ListingsEntry.CONTENT_ITEM_TYPE;
            case MAKE:
                return MakesEntry.CONTENT_TYPE;
            case MAKE_ID:
                return MakesEntry.CONTENT_ITEM_TYPE;
            case MODEL:
                return ModelsEntry.CONTENT_TYPE;
            case MODEL_ID:
                return ModelsEntry.CONTENT_ITEM_TYPE;
            case TRANSMISSION:
                return TransmissionsEntry.CONTENT_TYPE;
            case TRANSMISSION_ID:
                return TransmissionsEntry.CONTENT_ITEM_TYPE;
            case SETTING:
                return SettingsEntry.CONTENT_TYPE;
            case SETTING_ID:
                return SettingsEntry.CONTENT_ITEM_TYPE;
            case FEATURELISTING:
                return FeatureListingsEntry.CONTENT_TYPE;
            case FEATURELISTING_ID:
                return FeatureListingsEntry.CONTENT_ITEM_TYPE;
            case VEHICLE_IMAGE:
                return FeatureListingsEntry.CONTENT_TYPE;
            case VEHICLE_IMAGE_ID:
                return FeatureListingsEntry.CONTENT_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown Uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match  = sUriMatcher.match(uri);

        Uri returnUri;

        switch (match){
            case COLOR: {
                Log.i(LOG_TAG, "Inserting data in colors table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.COLORS, null, values);
                if ( _id > 0 )
                    returnUri = ColorsEntry.buildColorUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CATEGORY: {
                Log.i(LOG_TAG, "Inserting data in categories table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.CATEGORIES, null, values);
                if ( _id > 0 )
                    returnUri = CategoriesEntry.buildCategoryUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case CONDITION: {
                Log.i(LOG_TAG, "Inserting data in conditions table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.CONDITIONS, null, values);
                if ( _id > 0 )
                    returnUri = ConditionsEntry.buildConditionUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case FEATURE: {
                Log.i(LOG_TAG, "Inserting data in features table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.FEATURES, null, values);
                if ( _id > 0 )
                    returnUri = FeaturesEntry.buildFeatureUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case FUEL: {
                Log.i(LOG_TAG, "Inserting data in fuels table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.FUELS, null, values);
                if ( _id > 0 )
                    returnUri = FuelsEntry.buildFuelUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case LISTING: {
                Log.i(LOG_TAG, "Inserting data in listings table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.LISTINGS, null, values);
                if ( _id > 0 )
                    returnUri = ListingsEntry.buildListingUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case MAKE: {
                Log.i(LOG_TAG, "Inserting data in makes table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.MAKES, null, values);
                if ( _id > 0 )
                    returnUri = MakesEntry.buildMakeUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case MODEL: {
                Log.i(LOG_TAG, "Inserting data in models table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.MODELS, null, values);
                if ( _id > 0 )
                    returnUri = ModelsEntry.buildModelUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case TRANSMISSION: {
                Log.i(LOG_TAG, "Inserting data in transmissions table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.TRANSMISSIONS, null, values);
                if ( _id > 0 )
                    returnUri = TransmissionsEntry.buildTransmissionUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case SETTING: {
                Log.i(LOG_TAG, "Inserting data in settings table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.SETTINGS, null, values);
                if ( _id > 0 )
                    returnUri = SettingsEntry.buildSettingUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case FEATURELISTING: {
                Log.i(LOG_TAG, "Inserting data in feature_listings table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.FEATURE_LISTINGS, null, values);
                if ( _id > 0 )
                    returnUri = FeatureListingsEntry.buildFeatureListingUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            case VEHICLE_IMAGE: {
                Log.i(LOG_TAG, "Inserting data in vehicle_images table: " + uri + ". Values: " + values.toString());
                long _id = db.insert(DealerDbHelper.Tables.VEHICLE_IMAGES, null, values);
                if ( _id > 0 )
                    returnUri = FeatureListingsEntry.buildFeatureListingUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;

            }
            default:
                throw new UnsupportedOperationException("Unknown insert uri...: " + uri + ", Match is: " + match);
        }
        notifyChange(uri);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match  = sUriMatcher.match(uri);

        int rowsDeleted = 0;

        switch (match){
            case COLOR:
                Log.i(LOG_TAG, "Deleting data from colors table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.COLORS, selection, selectionArgs);
                break;

            case CATEGORY:
                Log.i(LOG_TAG, "Deleting data from categories table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.CATEGORIES, selection, selectionArgs);
                break;

            case CONDITION:
                Log.i(LOG_TAG, "Deleting data from conditions table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.CONDITIONS, selection, selectionArgs);
                break;

            case FEATURE:
                Log.i(LOG_TAG, "Deleting data from features table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.FEATURES, selection, selectionArgs);
                break;

            case FUEL:
                Log.i(LOG_TAG, "Deleting data from fuels table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.FUELS, selection, selectionArgs);
                break;

            case LISTING:
                Log.i(LOG_TAG, "Deleting data from listings table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.LISTINGS, selection, selectionArgs);
                break;

            case MAKE:
                Log.i(LOG_TAG, "Deleting data from makes table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.MAKES, selection, selectionArgs);
                break;

            case MODEL:
                Log.i(LOG_TAG, "Deleting data from models table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.MODELS, selection, selectionArgs);
                break;

            case TRANSMISSION:
                Log.i(LOG_TAG, "Deleting data from transmissions table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.TRANSMISSIONS, selection, selectionArgs);
                break;

            case SETTING:
                Log.i(LOG_TAG, "Deleting data from settings table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.SETTINGS, selection, selectionArgs);
                break;

            case FEATURELISTING:
                Log.i(LOG_TAG, "Deleting data from feature_listings table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.FEATURE_LISTINGS, selection, selectionArgs);
                break;

            case VEHICLE_IMAGE:
                Log.i(LOG_TAG, "Deleting data from vehicle_images table" + uri);
                rowsDeleted = db.delete(DealerDbHelper.Tables.VEHICLE_IMAGES, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Because a null deletes all rows
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    private void notifyChange(Uri uri) {
        // We only notify changes if the caller is not the sync adapter.
        // The sync adapter has the responsibility of notifying changes (it can do so
        // more intelligently than we can -- for example, doing it only once at the end
        // of the sync instead of issuing thousands of notifications for each record).
            Context context = getContext();
            context.getContentResolver().notifyChange(uri, null);

    }
}
