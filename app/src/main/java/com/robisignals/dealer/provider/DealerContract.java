package com.robisignals.dealer.provider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract class for interacting with provider
 */
public class DealerContract {

    public static final String CONTENT_AUTHORITY = "com.robisignals.dealer";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_COLOR = "colors";
    public static final String PATH_CATEGORY = "categories";
    public static final String PATH_CONDITION = "conditions";
    public static final String PATH_FEATURE = "features";
    public static final String PATH_FUEL = "fuels";
    public static final String PATH_LISTING = "listings";
    public static final String PATH_MAKE = "makes";
    public static final String PATH_MODEL = "models";
    public static final String PATH_TRANSMISSION = "transmissions";
    public static final String PATH_SETTINGS = "settings";
    public static final String PATH_FEATURE_LISTINGS = "feature_listings";
    public static final String PATH_VEHICLE_IMAGES = "vehicle_images";

    public static final String PATH_LISTINGS_SEARCH = "listings_search";

    public interface SyncColumns {
        String UPDATED = "updated";
    }

    interface ColorsColumns {
        String COLOR_ID = "color_id";
        String COLOR_NAME = "color_name";
        String CREATED_AT = "created_at";
    }

    interface CategoriesColumns {
        String CATEGORY_ID = "category_id";
        String CATEGORY_NAME = "category_name";
        String CREATED_AT = "created_at";
    }

    interface ConditionsColumns {
        String CONDITION_ID = "condition_id";
        String CONDITION_NAME = "condition_name";
        String CREATED_AT = "created_at";
    }

    interface FeaturesColumns {
        String FEATURE_ID = "feature_id";
        String FEATURE_NAME = "feature_name";
        String CREATED_AT = "created_at";
    }

    interface FuelsColumns {
        String FUEL_ID = "fuel_id";
        String FUEL_NAME = "fuel_name";
        String CREATED_AT = "created_at";
    }

    interface ListingsColumns {
        String LISTING_ID = "listing_id";
        String CATEGORY_ID = "category_id";
        String EXT_COLOR_ID = "ext_color_id";
        String INT_COLOR_ID = "int_color_id";
        String DOORS = "doors";
        String VIN_NUMBER = "vin_number";
        String ENGINE_SIZE = "engine_size";
        String CONDITION_ID = "condition_id";
        String EXTRA_DETAILS = "extra_details";
        String FEATURED = "featured";
        String FUEL_ID = "fuel_id";
        String MAKE_ID = "make_id";
        String MILEAGE = "mileage";
        String MODEL_ID = "model_id";
        String PRICE = "price";
        String REGISTRATION_YEAR = "registration_year";
        String TRANSMISSION_ID = "transmission_id";
        String ACTIVE = "active";
        String CREATED_AT = "created_at";
    }

    interface MakesColumns {
        String MAKE_ID = "make_id";
        String MAKE_NAME = "make_name";
        String CREATED_AT = "created_at";
    }

    interface ModelsColumns {
        String MODEL_ID = "model_id";
        String MAKE_ID = "make_id";
        String MODEL_NAME = "model_name";
        String CREATED_AT = "created_at";
    }

    interface TransmissionsColumns {
        String TRANSMISSION_ID = "transmission_id";
        String TRANSMISSION_NAME = "transmission_name";
        String CREATED_AT = "created_at";
    }

    interface SettingsColumns {
        String SETTING_ID = "setting_id";
        String COMPANY_NAME = "company_name";
        String COMPANY_DESCRIPTION = "company_description";
        String COMPANY_EMAIL = "company_email";
        String COMPANY_ADDRESS = "company_address";
        String COMPANY_CITY = "city";
        String COMPANY_STATE = "state";
        String COMPANY_POSTAL_CODE = "postal_code";
        String COMPANY_PHONE = "company_phone";
        String COMPANY_CURRENCY = "currency";
        String COMPANY_MILEAGE_UNITS = "mileage_units";
        String COMPANY_LONGITUDE = "longitude";
        String COMPANY_LATITUDE = "latitude";
        String COMPANY_FACEBOOK = "facebook";
        String COMPANY_TWITTER = "twitter";
        String COMPANY_WEBSITE = "website";
        String CREATED_AT = "created_at";
    }

    /**
     * A pivot table
     */
    interface FeatureListingsColumns {

        String FEATURE_LISTING_ID = "feature_listing_id";
        String FEATURE_ID = "feature_id";
        String LISTING_ID = "listing_id";
        String CREATED_AT = "created_at";

    }

    interface VehicleImagesColumns {
        String VEHICLE_IMAGE_ID = "vehicle_image_id";
        String FILE_NAME = "filename";
        String LISTING_ID = "listing_id";
        String CREATED_AT = "created_at";
    }

    public static class ColorsEntry implements ColorsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COLOR).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_COLOR;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_COLOR;

        public static Uri buildColorUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        /** Read {@link #COLOR_ID} from colors {@link Uri}. */
        public static String getColorId(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }


    public static class CategoriesEntry implements CategoriesColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CATEGORY).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORY;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_CATEGORY;

        public static Uri buildCategoryUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class ConditionsEntry implements ConditionsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CONDITION).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_CONDITION;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_CONDITION;

        //Build uri for a requested condition_id
        public static Uri buildConditionUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class FeaturesEntry implements FeaturesColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FEATURE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_FEATURE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_FEATURE;

        //Build uri for a requested feature_id
        public static Uri buildFeatureUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class FuelsEntry implements FuelsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FUEL).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_FUEL;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_FUEL;

        //Build uri for a requested fuel_id
        public static Uri buildFuelUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }

    public static class ListingsEntry implements ListingsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LISTING).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_LISTING;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_LISTING;

        //Build uri for a requested listing_id
        public static Uri buildListingUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }

    public static class MakesEntry implements MakesColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MAKE).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_MAKE;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_MAKE;

        //Build uri for a requested make_id
        public static Uri buildMakeUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }

    public static class ModelsEntry implements ModelsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MODEL).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_MODEL;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_MODEL;

        //Build uri for a requested model_id
        public static Uri buildModelUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }


    }

    public static class TransmissionsEntry implements TransmissionsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TRANSMISSION).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_TRANSMISSION;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_TRANSMISSION;

        //Build uri for a requested transmission_id
        public static Uri buildTransmissionUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class SettingsEntry implements SettingsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SETTINGS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_SETTINGS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_SETTINGS;

        //Build uri for a requested setting_id
        public static Uri buildSettingUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class FeatureListingsEntry implements FeatureListingsColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FEATURE_LISTINGS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_FEATURE_LISTINGS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_FEATURE_LISTINGS;

        //Build uri for a requested feature_listing_id
        public static Uri buildFeatureListingUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class VehicleImagesEntry implements VehicleImagesColumns, BaseColumns{

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_VEHICLE_IMAGES).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_VEHICLE_IMAGES;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_VEHICLE_IMAGES;

        //Build uri for a requested feature_listing_id
        public static Uri buildVehicleImageUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static class ListingsSearchEntry{
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LISTINGS_SEARCH).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_LISTINGS_SEARCH;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_LISTINGS_SEARCH;
    }
}
