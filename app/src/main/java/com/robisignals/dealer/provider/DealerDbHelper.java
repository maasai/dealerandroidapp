package com.robisignals.dealer.provider;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * TODO: Add a class header comment!
 */
public class DealerDbHelper extends SQLiteAssetHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "dealer.db";

    private final Context mContext;

    public interface Tables {
        String COLORS = "colors";
        String CATEGORIES = "categories";
        String CONDITIONS = "conditions";
        String FEATURES = "features";
        String FUELS = "fuels";
        String LISTINGS = "listings";
        String MAKES = "makes";
        String MODELS = "models";
        String TRANSMISSIONS = "transmissions";
        String SETTINGS = "settings";
        String FEATURE_LISTINGS = "feature_listings";
        String VEHICLE_IMAGES = "vehicle_images";

        String LISTINGS_SEARCH_QUERY = "listings "
                + "LEFT OUTER JOIN makes ON listings.make_id=makes.make_id "
                + "LEFT OUTER JOIN models ON listings.model_id=models.model_id "
                + "LEFT OUTER JOIN vehicle_images ON  listings.listing_id=vehicle_images.listing_id";

        String LISTINGS_JOIN_MAKES = "listings "
                + "LEFT OUTER JOIN makes ON listings.make_id=makes.make_id "
                + "LEFT OUTER JOIN models ON listings.model_id=models.model_id "
                + "LEFT OUTER JOIN vehicle_images ON  listings.listing_id=vehicle_images.listing_id GROUP BY listings.listing_id";

        String LISTINGS_ID_JOIN_ALL = "listings "
                + "LEFT OUTER JOIN vehicle_images ON listings.listing_id=vehicle_images.listing_id "
                + "LEFT OUTER JOIN colors AS c1 ON listings.ext_color_id=c1.color_id "
                + "LEFT OUTER JOIN settings "
                + "LEFT OUTER JOIN colors AS c2 ON listings.int_color_id=c2.color_id "
                + "LEFT OUTER JOIN transmissions ON listings.transmission_id=transmissions.transmission_id "
                + "LEFT OUTER JOIN fuels ON listings.fuel_id=fuels.fuel_id "
                + "LEFT OUTER JOIN conditions ON listings.condition_id=conditions.condition_id "
                + "LEFT OUTER JOIN categories ON listings.category_id=categories.category_id "
                + "LEFT OUTER JOIN models ON listings.model_id=models.model_id "
                + "LEFT OUTER JOIN makes ON listings.make_id=makes.make_id "
                + "LEFT OUTER JOIN features,feature_listings "
                + "WHERE listings.listing_id=feature_listings.listing_id "
                + "AND features.feature_id=feature_listings.feature_id AND listings._id=? GROUP BY listings.listing_id";
    }

    public DealerDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

   /* @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + Tables.COLORS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ColorsColumns.COLOR_ID + " TEXT NOT NULL,"
                + ColorsColumns.COLOR_NAME + " TEXT NOT NULL,"
                + ColorsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + ColorsColumns.COLOR_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + ColorsColumns.COLOR_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.CATEGORIES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + CategoriesColumns.CATEGORY_ID + " TEXT NOT NULL,"
                + CategoriesColumns.CATEGORY_NAME + " TEXT NOT NULL,"
                + CategoriesColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + CategoriesColumns.CATEGORY_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + CategoriesColumns.CATEGORY_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.CONDITIONS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ConditionsColumns.CONDITION_ID + " TEXT NOT NULL,"
                + ConditionsColumns.CONDITION_NAME + " TEXT NOT NULL,"
                + ConditionsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + ConditionsColumns.CONDITION_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + ConditionsColumns.CONDITION_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.FEATURES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FeaturesColumns.FEATURE_ID + " TEXT NOT NULL,"
                + FeaturesColumns.FEATURE_NAME + " TEXT NOT NULL,"
                + FeaturesColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + FeaturesColumns.FEATURE_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + FeaturesColumns.FEATURE_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.FUELS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FuelsColumns.FUEL_ID + " TEXT NOT NULL,"
                + FuelsColumns.FUEL_NAME + " TEXT NOT NULL,"
                + FuelsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + FuelsColumns.FUEL_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + FuelsColumns.FUEL_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.MAKES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + MakesColumns.MAKE_ID + " TEXT NOT NULL,"
                + MakesColumns.MAKE_NAME + " TEXT NOT NULL,"
                + MakesColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + MakesColumns.MAKE_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + MakesColumns.MAKE_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.TRANSMISSIONS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + TransmissionsColumns.TRANSMISSION_ID + " TEXT NOT NULL,"
                + TransmissionsColumns.TRANSMISSION_NAME + " TEXT NOT NULL,"
                + TransmissionsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + TransmissionsColumns.TRANSMISSION_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + TransmissionsColumns.TRANSMISSION_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.SETTINGS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SettingsColumns.SETTING_ID + " TEXT NOT NULL,"
                + SettingsColumns.COMPANY_NAME + " TEXT  NOT NULL,"
                + SettingsColumns.COMPANY_DESCRIPTION + " TEXT,"
                + SettingsColumns.COMPANY_ADDRESS + " TEXT,"
                + SettingsColumns.COMPANY_EMAIL + " TEXT,"
                + SettingsColumns.COMPANY_PHONE + " TEXT,"
                + SettingsColumns.COMPANY_CITY + " TEXT,"
                + SettingsColumns.COMPANY_STATE + " TEXT,"
                + SettingsColumns.COMPANY_POSTAL_CODE + " TEXT,"
                + SettingsColumns.COMPANY_CURRENCY + " TEXT NOT NULL,"
                + SettingsColumns.COMPANY_MILEAGE_UNITS + " TEXT NOT NULL,"
                + SettingsColumns.COMPANY_LONGITUDE + " TEXT,"
                + SettingsColumns.COMPANY_LATITUDE + " TEXT,"
                + SettingsColumns.COMPANY_FACEBOOK + " TEXT,"
                + SettingsColumns.COMPANY_TWITTER + " TEXT,"
                + SettingsColumns.COMPANY_WEBSITE + " TEXT,"
                + SettingsColumns.CREATED_AT + " DATETIME,"
                + "UNIQUE (" + SettingsColumns.SETTING_ID + ") ON CONFLICT REPLACE, "
                + "UNIQUE (" + SettingsColumns.COMPANY_NAME + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.MODELS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ModelsColumns.MODEL_ID + " TEXT NOT NULL,"
                + ModelsColumns.MAKE_ID + " TEXT NOT NULL,"
                + ModelsColumns.MODEL_NAME + " TEXT NOT NULL,"
                + ModelsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + " FOREIGN KEY (" + ModelsColumns.MAKE_ID + ") REFERENCES " +
                        Tables.MAKES + " (" + MakesColumns.MAKE_ID + "), "
                + "UNIQUE (" + ModelsColumns.MODEL_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.LISTINGS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ListingsColumns.LISTING_ID + " TEXT NOT NULL,"
                + ListingsColumns.INT_COLOR_ID + " TEXT NOT NULL,"
                + ListingsColumns.EXT_COLOR_ID + " TEXT NOT NULL,"
                + ListingsColumns.ENGINE_SIZE + " TEXT,"
                + ListingsColumns.DOORS + " TEXT,"
                + ListingsColumns.VIN_NUMBER + " TEXT,"
                + ListingsColumns.CATEGORY_ID + " TEXT NOT NULL,"
                + ListingsColumns.CONDITION_ID + " TEXT NOT NULL,"
                + ListingsColumns.FUEL_ID + " TEXT NOT NULL,"
                + ListingsColumns.MAKE_ID + " TEXT NOT NULL,"
                + ListingsColumns.MODEL_ID + " TEXT NOT NULL,"
                + ListingsColumns.TRANSMISSION_ID + " TEXT NOT NULL,"
                + ListingsColumns.ACTIVE + " TEXT NOT NULL,"
                + ListingsColumns.EXTRA_DETAILS + " TEXT NOT NULL,"
                + ListingsColumns.FEATURED + " TEXT NOT NULL,"
                + ListingsColumns.MILEAGE + " TEXT NOT NULL,"
                + ListingsColumns.PRICE + " TEXT NOT NULL,"
                + ListingsColumns.REGISTRATION_YEAR + " TEXT NOT NULL,"
                + ListingsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + ListingsColumns.LISTING_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.FEATURE_LISTINGS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + FeatureListingsColumns.FEATURE_LISTING_ID + " TEXT NOT NULL,"
                + FeatureListingsColumns.FEATURE_ID + " TEXT NOT NULL,"
                + FeatureListingsColumns.LISTING_ID + " TEXT NOT NULL,"
                + FeatureListingsColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + FeatureListingsColumns.FEATURE_LISTING_ID + ") ON CONFLICT REPLACE)");

        db.execSQL("CREATE TABLE " + Tables.VEHICLE_IMAGES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + VehicleImagesColumns.VEHICLE_IMAGE_ID + " TEXT NOT NULL,"
                + VehicleImagesColumns.FILE_NAME + " TEXT NOT NULL,"
                + VehicleImagesColumns.LISTING_ID + " TEXT NOT NULL,"
                + VehicleImagesColumns.CREATED_AT + " DATETIME NOT NULL,"
                + "UNIQUE (" + VehicleImagesColumns.VEHICLE_IMAGE_ID + ") ON CONFLICT REPLACE)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Tables.COLORS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.CONDITIONS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.FEATURES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.FUELS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.MAKES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.TRANSMISSIONS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.SETTINGS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.MODELS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.LISTINGS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.FEATURE_LISTINGS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.VEHICLE_IMAGES);

        onCreate(db);
    }*/
}
